/**
 * @author Aleksandra Mitrović
 * @date pet, 15. mart 2019.
 * vezbe4 / vezbe4.zadaci / Zadatak6.java
 * TODO
 */
package vezbe4.zadaci;

/**
 * 
 */
public class Zadatak6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (Dan d : Dan.values()) {
			System.out.println(d + " : " + d.ordinal());
		}
		
		// TODO: Ovu promenljivu menjati kako bi se testirali i ostali slucajevi.
		Dan izbor = Dan.PONEDELJAK;
//		
//		switch (izbor) {
//		case PONEDELJAK:
//			System.out.println("Prvi radni dan");
//			break;
//		case UTORAK:
//			System.out.println("Drugi radni dan");
//			break;
//		case SREDA:
//			System.out.println("Treci radni dan");
//			break;
//		case CETVRTAK:
//			System.out.println("Cetvrti radni dan");
//			break;
//		case PETAK:
//			System.out.println("Peti radni dan");
//			break;
//		case SUBOTA:
//			System.out.println("Mozda sesti radni dan");
//			break;
//		case NEDELJA:
//			System.out.println("Neradni dan");
//			break;
//		default:
//			System.out.println("Default blok");
//			break;
//		}
//		
		switch (izbor) {
		case PONEDELJAK:
		case UTORAK:
		case SREDA:
		case CETVRTAK:
		case PETAK:
			System.out.println("Radni dan, vreme je za posao!");
			break;
		case SUBOTA:
		case NEDELJA:
			System.out.println("Vikend, vreme je za odmor!");
			break;
		default:
			break;
		}

	}

}
