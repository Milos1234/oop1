/**
 * @author Aleksandra Mitrović
 * @date pet, 15. mart 2019.
 * vezbe4 / vezbe4.zadaci / Dan.java
 * TODO
 */
package vezbe4.zadaci;

/**
 * 
 * Po uzoru na primer sa: @see
 * <a href= "https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html">
 * https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html</a>
 */
public enum Dan {
	PONEDELJAK,
	UTORAK,
	SREDA,
	CETVRTAK,
	PETAK,
	SUBOTA,
	NEDELJA
}
