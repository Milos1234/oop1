/**
 * @author Aleksandra Mitrović
 * @date pet, 15. mart 2019.
 * vezbe4 / vezbe4.zadaci / Zadatak1.java
 * TODO
 */
package vezbe4.zadaci;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 */
public class Zadatak1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ArrayList<String> imena = new ArrayList<String>();
		
		imena.add("Sava");
		imena.add("Nena");
		imena.add("Kristina");
		imena.add("Viktor");
		imena.add("Valerija");
		imena.add("Milorad");
		
		imena.add(0, "Marko");
		imena.set(0, "Petar");
		imena.remove("Petar");
		System.out.println(imena.get(0));
		System.out.println(imena.size());
		
		// TODO: Zadatak 2
//		for (int i = 0; i < imena.size(); i++) {
//			System.out.println(imena.get(i));
//		}
		
		// TODO: Zadatak 3
//		for (String ime : imena) {
//			if (ime.equals("Viktor")) {
//				System.out.println("Viktor je pronadjen!");
//				break;
//			}
//		}
//		
//		// TODO: Zadatak 4
//		int index = 0;
//		while (index < imena.size()) {
//			System.out.println(imena.get(index));
//			index++;
//		}
//		
//		// TODO: Zadatak 5
//		/*
//		 * Ovde uočavamo da se telo do petlje bar jednom izvrši,
//		 * čak iako nije ispunjen uslov u while petlji za dalje izvršavanje.
//		 */
//		index = 0;
//		do {
//			System.out.println("Trenutni indeks je: " + index);
//			index++;
//		} while (index < imena.size());
//		
//		// TODO: Primer iterisanja uz pomoc iteratora
//		System.out.println("Sa iteratorom:");
//		Iterator<String> iterator = imena.iterator();
//		while (iterator.hasNext()) {
//			System.out.println(iterator.next());
//		}
	}
}
