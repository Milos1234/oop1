/**
 * @author Aleksandra Mitrović
 * @date pet, 15. mart 2019.
 * vezbe4 / vezbe4.zadaci / Zadatak11.java
 * TODO
 */
package vezbe4.zadaci;

import java.util.ArrayList;

/**
 * 
 */
public class Zadatak11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String naziv = null;
		try {
			System.out.println(naziv.charAt(0));
		} catch (NullPointerException e) {
			System.out.println("Nemoguće je pozivati nestatičke metode nad nepostojećom instancom (null vrednošću)!");
		}
		// FIXME: Inace nije dobra praksa da se staticke metode (u ovom primeru valueOf)
		// pozivaju nad instancama, već se one pozivaju nad klasom.
		naziv.valueOf(5.5f);
		System.out.println("Ovo je proslo!");
		// Umesto ovoga ispravno bi bilo:
		String.valueOf(5.5f); // Pozivanje nad klasom String
	}

}
