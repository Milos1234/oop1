/**
 * @author Aleksandra Mitrović
 * @date pet, 15. mart 2019.
 * vezbe4 / vezbe4.zadaci / Zadatak10.java
 * TODO
 */
package vezbe4.zadaci;

/**
 * 
 */
public class Zadatak10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Jednolinijski nacin za definisanje niza od pet vrednosti
		int[] niz = new int[] {5, 3, 6, 18, 19};
		// Drugi nacin
//		int[] niz = { 5, 3, 6, 18, 19 };
		
		try {
			System.out.println(niz[5]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Nemoguce je indeksirati niz indeksom 5.");
		}

	}

}
