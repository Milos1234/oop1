/**
 * @author Aleksandra Mitrović
 * @date pet, 15. mart 2019.
 * vezbe4 / vezbe4.zadaci / Zadatak9.java
 * TODO
 */
package vezbe4.zadaci;

/**
 * 
 */
public class Zadatak9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// TODO: Probati isto sa promenljivama tipa double i bez try bloka
		int deljenik = 10;
		int delilac = 0;
		
		try {
			System.out.println(deljenik/delilac);
		} catch (ArithmeticException e) {
			System.out.println("Nemoguce je ceo broj deliti nulom.");
		}
	
	}

}
