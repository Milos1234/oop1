package cetvrtizadatak;

import java.io.*;

public class Zadatak42 {
	public static void main(String[] args) throws IOException {

		FileReader in = null;
		FileWriter out1 = null;
		FileWriter out2 = null;

		byte[] niz = { 'b', 'y', 't', 'e', ' ', 'u', ' ', 'T', 'E', 'K', 'S', 'T', 'd', 'a', 't' };
		String str = " Upis stringa u tekstualnu datoteku";
		try {
			in = new FileReader("dat1.txt");
			out1 = new FileWriter("dat1.txt");
			out2 = new FileWriter("dat2.txt");
			int c;
			String s = new String(niz);
			out1.write(s);
			out1.write(str);
			out1.write('%');
			out1.close();

			while ((c = in.read()) != -1) {
				out2.write(c);
				System.out.print((char) c);
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out1 != null) {
				out1.close();
			}
			if (out2 != null) {
				out2.close();
			}
		}
	}
}