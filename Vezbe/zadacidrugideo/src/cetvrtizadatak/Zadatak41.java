package cetvrtizadatak;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Zadatak41 {
	public static void main(String[] args) throws IOException {
		FileInputStream in = null;
		FileOutputStream out1 = null;
		FileOutputStream out2 = null;

		byte[] bajtniz = { 'd', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a', 't', 'e', 'd' };
		String str = "Primer upisa stringa u binarnu datoteku i znak %";
		int c;

		try {
			in = new FileInputStream("dat1.txt");
			out1 = new FileOutputStream("dat1.txt");
			out2 = new FileOutputStream("dat2.txt");
			out1.write(bajtniz);
			byte s [] = str.getBytes();
			out1.write(s);
			out1.write('%');

			while ((c = in.read()) != -1) {
				out2.write(c);
				System.out.print((char) c + " ");
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out1 != null) {
				out1.close();
			}
			if (out2 != null) {
				out2.close();
			}
		}
	}
}
