package drugizadatak;

import java.util.Hashtable;

import org.json.JSONObject;

public class Zadatak21 {

	public static void main(String[] args) {
		Hashtable<String, Double> voce = new Hashtable<String, Double>();
		String voceJson = "{\"Jabuke\": 87.0, \"Banane\": 112.0, \"Ananas\": 205.0, \"Badem\": 399.0}";
		JSONObject voceObject = new JSONObject(voceJson);

		for (String key : voceObject.keySet()) {
			voce.put(key, voceObject.getDouble(key));
		}

		Hashtable<String, Double> voceUslov = new Hashtable<String, Double>();

		for (String key : voce.keySet()) {
			if (voce.get(key) < 100.) {
				voceUslov.put(key, voce.get(key));
			}
		}

		JSONObject voceUslovObject = new JSONObject(voceUslov);
		System.out.println(voceUslovObject.toString(4));
	}

}
