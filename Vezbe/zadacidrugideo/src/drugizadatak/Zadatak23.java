package drugizadatak;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadatak23 {

	public static void main(String[] args) {
		HashMap<Integer, String> osobe = new HashMap<Integer, String>();
		osobe.put(100, "Ana");
		osobe.put(101, "Veljko");
		osobe.put(102, "Rajko");
		osobe.put(103, "Mila");
		Scanner in = new Scanner(System.in);
		System.out.print("Unesite kljuc: ");
		try {
			Integer kljuc = in.nextInt();
			in.nextLine(); // pokupimo \n koji zaostane posle nextInt.
			System.out.print("Unesite vrednost: ");
			String vrednost = in.nextLine();
			
			System.out.println("Rezultat: " + isOsoba(osobe, kljuc, vrednost));
		} catch (InputMismatchException e) {
			System.out.println("Nije unet ceo broj");
		}
		in.close();
	}

	public static boolean isOsoba(HashMap<Integer, String> osobe, Integer kljuc, String vrednost) {

		if (osobe.containsKey(kljuc)) {
			return true;
		}
		if (osobe.containsValue(vrednost)) {
			return true;
		}
		return false;
	}

}
