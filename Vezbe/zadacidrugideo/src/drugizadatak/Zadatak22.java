package drugizadatak;

import java.util.HashMap;

import org.json.JSONObject;

public class Zadatak22 {

	public static void main(String[] args) {
		HashMap<Integer, String> gradovi = new HashMap<Integer, String>();
		JSONObject gradoviJson = new JSONObject();
		/*
		 * elementi: (11030, Beograd - Cukarica), (11060, Beograd - Palilula), (11080,
		 * Beograd - Zemun), (21000, Novi Sad), (15000, Sabac), (36000, Kraljevo),
		 * (22406, Irig), (11500, Obrenovac)
		 */
		/*
		 * Kada se pravi json objekat, kljuc je obavezno string!
		 */
		gradoviJson.accumulate("11030", "Beograd - Cukarica");
		gradoviJson.accumulate("11060", "Beograd - Palilula");
		gradoviJson.accumulate("11080", "Beograd - Zemun");
		gradoviJson.accumulate("21000", "Novi Sad");
		gradoviJson.accumulate("15000", "Sabac");
		gradoviJson.accumulate("36000", "Kraljevo");
		gradoviJson.accumulate("22406", "Irig");
		gradoviJson.accumulate("11500", "Obrenovac");

		for (String key : gradoviJson.keySet()) {
			// TODO: Ovde se kljuc koji je String mora pretvoriti u Integer
			gradovi.put(Integer.parseInt(key), gradoviJson.getString(key));
		}

		// FIXME: pretvaranje primitivnog tipa int u Integer raditi pomocu staticke
		// metode valueOf.
		if (!gradovi.containsKey(Integer.valueOf(11090))) {
			gradovi.put(Integer.valueOf(11090), "Beograd - Rakovica");
		}

		HashMap<Integer, String> gradoviUslov = new HashMap<Integer, String>();
		for (Integer key : gradovi.keySet()) {
			/*
			 * Operatori rade za primitivne klase i za njihove vrapere
			 */
			if (key >= 11000 && key < 12000) {
				gradoviUslov.put(key, gradovi.get(key));
			}
			/*
			 * Za ostale klase, mora se impelementirati interfejs Comparable i pozivati
			 * metod compareTo kako bi se vrsilo poredjenje. Evo i primera ispod.
			 */
//			if (key.compareTo(Integer.valueOf(11000)) > -1 && key.compareTo(Integer.valueOf(12000)) < 0) {
//				gradoviUslov.put(key, gradovi.get(key));
//			}
		}

		JSONObject gradoviUslovObject = new JSONObject(gradoviUslov);
		System.out.println(gradoviUslovObject.toString(4));

	}

}
