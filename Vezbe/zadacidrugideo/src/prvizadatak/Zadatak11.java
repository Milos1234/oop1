package prvizadatak;

import java.util.*;

public class Zadatak11 {
	public static void main(String[] args) {
		ArrayList<String> imena = new ArrayList<>();
		ArrayList<Integer> imenaDuzina = new ArrayList<>();
		Scanner in = new Scanner(System.in);
		int brojImena;

		while (true) {
			System.out.print("Unesite ceo pozitivan broj: ");
			String broj = in.nextLine();
			try {
				brojImena = Integer.parseInt(broj);
				if (brojImena < 0) {
					System.out.println("Broj imena mora biti pozitivan broj!");
					continue;
				}
				break;
			} catch (NumberFormatException e) {
				System.out.println("Nije unet ceo broj!");
			}
		}

		// TODO: Drugi nacin za resavanje unosa celog broja sa obradom izuzetaka
		// potrbno je zakomentarisati prethodnu while petlju, a otkomentarisati ovu.
//		while (true) {
//			try {
//				System.out.print("Unesite ceo pozitivan broj: ");
//				brojImena = in.nextInt();
//				/*
//				 * Posle nextInt mora se pozvati metoda nextLine kako bi pokupila zaostali novi
//				 * red (\n) koji je dobijen potvrdom (pritiskom enter tastera) za unos broja.
//				 */
//				in.nextLine();
//				if (brojImena < 0) {
//					System.out.println("Broj imena mora biti pozitivan broj!");
//					continue;
//				} else {
//					break;
//				}
//			} catch (InputMismatchException e) {
//				in.nextLine(); // ponovo kupimo enter (\n)
//				System.out.println("Nije unet ceo broj!");
//			}
//		}

		for (int i = 1; i <= brojImena; i++) {
			System.out.print("Unesite jedno ime: ");
			String ime = in.nextLine();
			imena.add(ime);
		}
		in.close();

		for (String s : imena) {
			imenaDuzina.add(s.length());
		}
		System.out.println(imena);
		System.out.println(imenaDuzina);
	}
}
