package prvizadatak;

import java.util.ArrayList;

import org.json.JSONArray;

public class Zadatak13 {
	public static void main(String[] args) {
		ArrayList<String> imena = new ArrayList<String>();
		/*
		 * Kako bismo formirali string koji u sebi sadrzi stringove moramo upotrebiti
		 * escape operator (\) koji ne posmatra znak navoda (") kao početak/kraj stringa.
		 */
		String imenaJson = "[\"Pera\", \"Mika\", \"Zika\", \"Mara\", \"Nemanja\"]";
		JSONArray jsonArray = new JSONArray(imenaJson);
		System.out.println(jsonArray);
		for (Object o : jsonArray) {
			imena.add((String) o);
		}
		if (!imena.contains("Ivan")) {
			imena.add("Ivan");
		}
		ArrayList<String> imenaUslov = new ArrayList<String>();
		for (String ime : imena) {
			if (ime.length() < 5) {
				imenaUslov.add(ime);
			}
		}
		JSONArray imenaUslovArray = new JSONArray(imenaUslov);
		String imenaUslovString = imenaUslovArray.toString();
		System.out.println(imenaUslovString);
	}
}
