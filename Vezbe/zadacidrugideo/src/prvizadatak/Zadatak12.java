package prvizadatak;

import java.util.*;

public class Zadatak12 {
	public static void main(String[] args) {
		ArrayList<Tacka> tacke = new ArrayList<Tacka>();
		ArrayList<Tacka> tackeUslov = new ArrayList<Tacka>();
		Scanner in = new Scanner(System.in);
		int n = 5;
		Tacka tacka;
		try {
			for (int i = 0; i < n; i++) {
				System.out.print("Unesite koordinatu x: ");
				int x = in.nextInt();
				System.out.print("Unesite koordinatu y: ");
				int y = in.nextInt();
				tacka  = new Tacka();
				tacka.x = x;
				tacka.y = y;
				tacke.add(tacka);
			}
		} catch (InputMismatchException e) {
			System.out.println("Koordinata taÄ�ke nije ceo broj!");
		}
		in.close();

		for (Tacka t : tacke) {
			System.out.println("lista tacke: (" + t.x + "," + t.y + ")");
		}
		Iterator<Tacka> itr = tacke.iterator();
		while (itr.hasNext()) {
			Tacka t = itr.next();
			if (t.x * t.x + t.y * t.y < 100) {
				tackeUslov.add(t);
			}
		}
		for (Tacka t : tackeUslov) {
			System.out.println("lista tackeUslov: (" + t.x + "," + t.y + ")");
		}
	}
}