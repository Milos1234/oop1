package trecizadatak;

import java.util.*;

public class Zadatak31 {
	public static void main(String[] args) {
		Integer arr[] = { 5, 6, 7, 8, 1, 2, 3, 4, 3 };
		Set<Integer> brojevi = new HashSet<>(Arrays.asList(arr));
		Set<Integer> parni = new HashSet<>();
		Iterator<Integer> itr = brojevi.iterator();
		while (itr.hasNext()) {
			Integer br = itr.next();
			if (br % 2 == 0) {
				parni.add(br);
				System.out.println(br);
			}
		}
	}
}