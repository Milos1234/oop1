package trecizadatak;

import java.util.*;

public class Zadatak32 {
	public static void main(String[] args) {
		Set<Tacka> tacke = new HashSet<>();
		Set<Integer> skupx = new HashSet<>();
		Set<Integer> skupy = new HashSet<>();

		tacke.add(new Tacka(1, 1));
		tacke.add(new Tacka(2, 2));
		tacke.add(new Tacka(3, 3));
		tacke.add(new Tacka(4, 4));
		tacke.add(new Tacka(5, 5));

		for (Tacka t : tacke) {
			skupx.add(t.x);
			skupy.add(t.y);
		}
		for (Tacka t : tacke) {
			System.out.println(t.x + "tačke" + t.y);
		}
		for (Integer i : skupx) {
			System.out.println(i + "koordinata x");
		}
		for (Integer i : skupy) {
			System.out.println(i + "koordinata y");
		}
	}
}
