package grupa2.zadatak1;

public class Osoba {
	int starost;
	String imePrezime;
	
	/**
	 * Konstruktor sa parametrima.
	 * @param starost - koliko godina ima osoba
	 * @param imePrezime
	 */
	public Osoba(int starost, String imePrezime) {
		this.starost = starost;
		this.imePrezime = imePrezime;
	}
	
	/**
	 * Metoda koja kreira String konkatenacijom vrednosti svih atributa.
	 * @return String spojenih vrednosti atributa
	 */
	public String predstaviSe() {
		return starost + " " + imePrezime;
	}
}
