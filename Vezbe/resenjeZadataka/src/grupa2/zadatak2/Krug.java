package grupa2.zadatak2;

public class Krug {
	private double poluprecnik;

	public Krug(double poluprecnik) {
		this.poluprecnik = poluprecnik;
	}

	public double getPovrsina() {
		return poluprecnik * poluprecnik * Math.PI;
	}
	
	public double getPoluprecnik() {
		return poluprecnik;
	}
}
