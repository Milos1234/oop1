package grupa2.zadatak2;

public class Test22 {

	public static void main(String[] args) {
		Valjak v1 = new Valjak(5.2, 10.0);
		System.out.println("Povrsina: " + v1.getPovrsina() + " zapremina: " + v1.getZapremina());
		System.out.println("Rezultat metode jednakost: " + v1.jednakost());
		Valjak v2 = new Valjak(1.0, 1.0);
		System.out.println("Rezultat metode jednakost: " + v2.jednakost());
	}

}
