package grupa2.zadatak2;

/**
 * 
 */
public class Valjak extends Krug {
	private double visina;
	
	public Valjak(double poluprecnik, double visina) {
		super(poluprecnik);
		this.visina = visina;
	}
	
	public double getZapremina() {
		return getPovrsina() * visina;
	}
	
	public boolean jednakost() {
		boolean rezultat = false;
		if (Double.compare(getPoluprecnik(), visina) == 0) {
			rezultat = true;
		}
		return rezultat;
	}
}
