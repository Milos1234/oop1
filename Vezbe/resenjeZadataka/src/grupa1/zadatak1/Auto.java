package grupa1.zadatak1;

/**
 * Klasa Auto.
 */
public class Auto {
	// atributi klase
	String marka;
	int broj;
	
	/**
	 * Konstruktor bez parametara.
	 */
	public Auto() {}
	
	/**
	 * Konstruktor sa parametrima.
	 * @param marka
	 * @param broj
	 */
	public Auto(String marka, int broj) {
		this.marka = marka;
		this.broj = broj;
	}

	/**
	 * Konstruktor kopije.
	 * @param auto
	 */
	public Auto(Auto auto) {
		this.marka = auto.marka;
		this.broj = auto.broj;
	}
	
	public void ispis(Vrsta v) {
		System.out.println("Automobil marke: " + marka + " broj: " + broj);
	}
}
