package grupa1.zadatak1;

public class Test11 {

	public static void main(String[] args) {
		Auto a1 = new Auto();
		//a1.marka = "Ford";
		//a1.broj = 2345;
		Auto a2 = new Auto("Fiat", 23876549);
		Auto a3 = new Auto(a2);
	    System.out.println("Auto a1: " + a1.marka + "; "+ a1.broj);
	    System.out.println("Auto a2: " + a2.marka + "; "+ a2.broj);
	    System.out.println("Tackaa3: " + a3.marka + "; "+ a3.broj);
	}

}
