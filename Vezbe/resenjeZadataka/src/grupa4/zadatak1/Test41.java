package grupa4.zadatak1;

public class Test41 {

	public static void main(String[] args) {
		SwissBanka banka = new SwissBanka();
		banka.naziv = "Banca Intesa";
		banka.adresa = "Novi Beograd, ul. Milentija Popovića 7b";
		System.out.println(banka.podaciBanka());
		System.out.println("Kamata je: " + banka.kamata());
		SwissBanka druga = new SwissBanka();
		druga.naziv = "UniCredit";
		druga.adresa = "Narodnog fronda bb, Novi Sad.";
		SwissBanka[] banke = new SwissBanka[2];
		banke[0] = banka;
		banke[1] = druga;
		System.out.println(banka.sadrziBanku(banke));
	}

}
