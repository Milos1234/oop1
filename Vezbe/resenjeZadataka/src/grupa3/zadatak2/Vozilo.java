package grupa3.zadatak2;

public abstract class Vozilo {
	protected Osoba vlasnik;
	protected String registarskaOznaka;
	protected String brojSasije;

	public Vozilo() {}

	public Vozilo(Osoba vlasnik, String registarskaOznaka, String brojSasije) {
		this.vlasnik = vlasnik;
		this.registarskaOznaka = registarskaOznaka;
		this.brojSasije = brojSasije;
	}
	
	public abstract void registruj(Osoba vlasnik);
}
