package grupa3.zadatak1;

public abstract class Osoba {
	protected String ime;
	private String pol;

	public Osoba(String ime, String pol) {
		this.ime = ime;
		this.pol = pol;
	}

	public abstract void zaposlen();

	@Override
	public String toString() {
		return "Ime=" + this.ime + "; Pol=" + this.pol;
	}

	public void promenaImena(String novoIme) {
		this.ime = novoIme;
	}
}
