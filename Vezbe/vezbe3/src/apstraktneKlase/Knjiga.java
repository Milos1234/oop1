/**
 * @author Aleksandra Mitrović
 * @date čet, 14. mart 2019.
 * vezbe3 / apstraktneKlase / Knjiga.java
 * TODO
 */
package apstraktneKlase;

/**
 * 
 */
public class Knjiga extends UmetnickoDelo {
	
	private String naslov;
	private String isbn;
	private String izdavac;
	private int brojStrana;
	private boolean tvrdiPovez;
	/**
	 * Konstruktor bez parametara
	 */
	public Knjiga() {
		super();
	}

	/**
	 * 
	 * @param autori
	 * @param godinaIzdanja
	 * @param naslov
	 * @param isbn
	 * @param izdavac
	 * @param brojStrana
	 * @param tvrdiPovez
	 */
	public Knjiga(String[] autori, int godinaIzdanja, int ocena, String naslov, String isbn, String izdavac, int brojStrana,
			boolean tvrdiPovez) {
		super(autori, godinaIzdanja, ocena);
		this.naslov = naslov;
		this.isbn = isbn;
		this.izdavac = izdavac;
		this.brojStrana = brojStrana;
		this.tvrdiPovez = tvrdiPovez;
	}

	
	public String getNaslov() {
		return naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getIzdavac() {
		return izdavac;
	}

	public void setIzdavac(String izdavac) {
		this.izdavac = izdavac;
	}

	public int getBrojStrana() {
		return brojStrana;
	}

	public void setBrojStrana(int brojStrana) {
		this.brojStrana = brojStrana;
	}

	public boolean isTvrdiPovez() {
		return tvrdiPovez;
	}

	public void setTvrdiPovez(boolean tvrdiPovez) {
		this.tvrdiPovez = tvrdiPovez;
	}

	/* (non-Javadoc)
	 * @see apstraktneKlase.UmetnickoDelo#sviPodaci()
	 */
	@Override
	public String sviPodaci() {
		return osnovniPodaci() + " naslov: " + naslov + " ISBN: " + isbn + " izdavac: "+ izdavac + " broj strana:" +
				brojStrana + " ima tvrdi povez: " + tvrdiPovez;
	}

}
