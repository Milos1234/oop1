/**
 * @author Aleksandra Mitrović
 * @date čet, 14. mart 2019.
 * vezbe3 / apstraktneKlase / Zadatak5.java
 * TODO
 */
package apstraktneKlase;

/**
 * 
 */
public class Zadatak5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * FIXME Ovo ne radi iz vise razloga: 
		 * 	1. Apstraktne klase se ne instanciraju, 
		 * 	2. Konstruktor ove klase je protected (ovde nije vidljiv).
		 */
//		UmetnickoDelo ud;
//		ud = UmetnickoDelo();
		UmetnickoDelo slika = new Slika(new String[] { "Marko Markovic" }, 2002, 15, "ulje na platnu",
				new float[] { 10.2f, 3.8f });
		// Ovde smo mogli staviti i Knjiga knjiga = new Knjiga(...);
		UmetnickoDelo knjiga = new Knjiga(new String[] { "Pera Peric", " Jova Jovic" }, 2001, 20, "Knjiga u svemiru",
				"943-84-1355-54", "Mladost", 200, true);

		System.out.println(slika.osnovniPodaci());
		System.out.println(knjiga.osnovniPodaci());

		System.out.println(slika.sviPodaci());
		System.out.println(knjiga.sviPodaci());

		// Ovu metodu NE MOZEMO pozvati nad instancom UmetnickoDelo
		// jer je definisana u klasi Slika
//		slika.visina();
		// Iz tog razloga potrebno je da upotrebimo cast operator da bismo
		// dobili sliku tipa Slika.
		System.out.println(((Slika) slika).visina());
		System.out.println(((Slika) slika).sirina());
//		
//		// TODO: Iz 6. zadatka
		slika.glasajZa();
		slika.stampajOcenu();
		knjiga.glasajProtiv();
		knjiga.stampajOcenu();
	}
}
