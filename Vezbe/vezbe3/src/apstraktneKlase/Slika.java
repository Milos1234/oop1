/**
 * @author Aleksandra Mitrović
 * @date čet, 14. mart 2019.
 * vezbe3 / apstraktneKlase / Slika.java
 * TODO
 */
package apstraktneKlase;

/**
 * 
 */
public class Slika extends UmetnickoDelo {
	
	private String tip; // ulje na platnu, mozaik, goblen, ...
	private float[] dimenzije; // sirina, visina
	
	public Slika() {
		super();
	}

	public Slika(String[] autori, int godinaIzdanja, int ocena, String tip, float[] dimenzije) {
		super(autori, godinaIzdanja, ocena);
		this.tip = tip;
		this.dimenzije = dimenzije;
	}
	
	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public float[] getDimenzije() {
		return dimenzije;
	}

	public void setDimenzije(float[] dimenzije) {
		this.dimenzije = dimenzije;
	}
	
	/* (non-Javadoc)
	 * @see apstraktneKlase.UmetnickoDelo#sviPodaci()
	 */
	@Override
	public String sviPodaci() {
		// TODO Auto-generated method stub
		return osnovniPodaci() + " tip: " + tip + " dimenzije: " + dimenzije;
	}

	public float sirina() {
		return dimenzije[0];
	}
	
	public float visina() {
		return dimenzije[1];
	}

}
