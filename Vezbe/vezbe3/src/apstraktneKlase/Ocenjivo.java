/**
 * @author Aleksandra Mitrović
 * @date pet, 15. mart 2019.
 * vezbe3 / apstraktneKlase / Ocenjivo.java
 * TODO
 */
package apstraktneKlase;

/**
 * Ovaj interfejs bi trebale da implementiraju sve klase koje
 * mogu da se ocene.
 */
public interface Ocenjivo {
	
	public void glasajZa();
	public void glasajProtiv();
	public void stampajOcenu();
}
