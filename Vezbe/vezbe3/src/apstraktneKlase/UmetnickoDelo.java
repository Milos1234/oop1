/**
 * @author Aleksandra Mitrović
 * @date čet, 14. mart 2019.
 * vezbe3 / apstraktneKlase / UmetnickoDelo.java
 * TODO
 */
package apstraktneKlase;

/**
 * Implementacija interfejsa je dodata tek u 6. zadatku,
 * kao i atribut ocena.
 */
public abstract class UmetnickoDelo implements Ocenjivo {
	
	protected String[] autori;
	protected int godinaIzdanja;
	// TODO: dodato u 6. zadatku
	protected int ocena;
	
	/**
	 * Konstruktor bez parametara.
	 */
	protected UmetnickoDelo() {
		super();
	}
	
	/**
	 * Konstruktor sa parametrima.
	 * @param autori
	 * @param godinaIzdanja
	 * @param ocena
	 */
	protected UmetnickoDelo(String[] autori, int godinaIzdanja, int ocena) {
		super();
		this.autori = autori;
		this.godinaIzdanja = godinaIzdanja;
		this.ocena = ocena;
	}

	public String[] getAutori() {
		return autori;
	}

	public void setAutori(String[] autori) {
		this.autori = autori;
	}

	public int getGodinaIzdanja() {
		return godinaIzdanja;
	}

	public void setGodinaIzdanja(int godinaIzdanja) {
		this.godinaIzdanja = godinaIzdanja;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	// Primer upotrebe final modifikatora
	// Ova metoda se ne moze redefinisati u klasama koje nasledjuju UmetnickoDelo.
	public final String osnovniPodaci() {
		return "Autori: " + autori + ", godina izdanja: " + godinaIzdanja;
	}
	
	// Ova apstraktna metoda nema telo u ovoj klasi, ali
	// klase naslednice ce prilikom nasledjivanja redefinisati ovu metodu
	// i dodati joj telo, odnosno specificirati kako radi.
	public abstract String sviPodaci();

	// U nastavku slede metode koje su redefinisane i koje su
	// dobijene implementacijom interfejsa Ocenjivo.
	@Override
	public void glasajZa() {
		this.ocena++;
	}

	@Override
	public void glasajProtiv() {
		this.ocena--;
	}

	@Override
	public void stampajOcenu() {
		System.out.printf("Ocena umetnickog dela je: %d\n", ocena);
	}
}
