/**
 * @author Aleksandra Mitrović
 * @date čet, 14. mart 2019.
 * vezbe3 / objectKlasa / Zadatak1.java
 * TODO
 */
package objectKlasa;

/**
 * 
 */
public class Zadatak1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Knjiga javaProgramiranje = new Knjiga("dr Dejan Zivkovic", "Java programiranje", "978-86-7912-521-7", 2019);
		Knjiga algoritmiIStrukture = new Knjiga("dr Dejan Zivkovic", "Uvod u algoritme i strukture podataka",
				"978-86-7912-572-9", 2018);
		System.out.println(javaProgramiranje.toString());
		System.out.println(algoritmiIStrukture); // iako ovde nismo naveli toString, stampanje na standardni
												 // izlaz ce pokusati da nadje ovu metodu u nasoj klasi i
												 // pozvati je
	}

}
