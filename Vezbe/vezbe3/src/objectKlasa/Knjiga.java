/**
 * @author Aleksandra Mitrović
 * @date čet, 14. mart 2019.
 * vezbe3 / objectKlasa / Knjiga.java
 * TODO
 */
package objectKlasa;

import java.util.Objects;

/**
 * Klasa koja predstavlja knjigu
 */
public class Knjiga {
	private String autor;
	private String naslov;
	private String isbn;
	private int godinaIzdanja;

	/**
	 * Konstruktor bez parametara.
	 */
	public Knjiga() {
		super();
	}

	/**
	 * Konstruktor klase Knjiga sa parametrima.
	 * 
	 * @param autor
	 * @param naslov
	 * @param isbn
	 * @param godinaIzdanja
	 */
	public Knjiga(String autor, String naslov, String isbn, int godinaIzdanja) {
		super();
		this.autor = autor;
		this.naslov = naslov;
		this.isbn = isbn;
		this.godinaIzdanja = godinaIzdanja;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getNaslov() {
		return naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getGodinaIzdanja() {
		return godinaIzdanja;
	}

	public void setGodinaIzdanja(int godinaIzdanja) {
		this.godinaIzdanja = godinaIzdanja;
	}

	@Override
	public String toString() {
		return "Knjiga [autor=" + autor + ", naslov=" + naslov + ", isbn=" + isbn + ", godinaIzdanja=" + godinaIzdanja
				+ "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(autor, godinaIzdanja, isbn, naslov);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) { // ako su objekti na istoj referenci
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) { // ako objekti nisu istog tipa (klase)
			return false;
		}
		
		// Pretvaranje (eng. cast) promenljive obj tipa Object u tip Knjiga
		// ako je prošao prethodni if znamo da ovo možemo da uradimo.
		Knjiga other = (Knjiga) obj; 
		// FIXME: ova naredba je za Zadatak 2
		return Objects.equals(autor, other.autor);
		// FIXME: ova naredba je za Zadatak 3
//		return Objects.equals(isbn, other.isbn);
		// -------------------------------------
		// FIXME: Dakle, najčešće, ako NE postoji neki atribut (poput. ISBN-a za knjigu) koji na nivou
		// svih instanci klase ima različitu vrednost, se vrši poređenje svih vrednosti atributa, kao u nastavku:
//		return Objects.equals(autor, other.autor) && godinaIzdanja == other.godinaIzdanja
//				&& Objects.equals(isbn, other.isbn) && Objects.equals(naslov, other.naslov);
		// -------------------------------------
	}

}
