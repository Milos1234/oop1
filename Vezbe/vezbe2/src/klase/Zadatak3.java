/**
 * @author Aleksandra Mitrović
 * @date pon, 4. mart 2019.
 * vezbe2 / klase / Zadatak3.java
 * TODO
 */
package klase;

/**
 * Kreirati klasu Zadatak3 sa main metodom.
 * Deklarisati niz 10 celih brojeva.
 * Definisati niz String-ova koji sadrži tri imena.
 * Dobaviti dužinu niza i ispisati na konzolu.
 * Dodati ime Zika na četvrtu poziciju. Štampati novododato ime.
 * TODO: Cilj zadatka je razgraničiti termine deklaracija i definicija,
 *       kao i rad sa nizovima.
 */
public class Zadatak3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nizBrojeva; // ovo je deklaracija
		nizBrojeva = new int[10]; // definicija niza brojeva duzine 10
		System.out.println(nizBrojeva.length);
		String[] imena = {"Marko", "Pera", "Mika"}; // ovo je definicija (deklaracija sa dodelom vrednosti).
		imena[0] = "Zika";
		System.out.println(imena[0]);
		imena[4] = "Nemanja"; // FIXME: u debug rezimu pokrenuti ovo i utvrditi sta se desava
		System.out.println(imena[4]);
	}

}
