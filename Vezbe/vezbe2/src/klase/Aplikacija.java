/**
 * @author Aleksandra Mitrović
 * @date ned, 3. mart 2019.
 * vezbe2 / klase / Aplikacija.java
 * TODO
 */
package klase;

/**
 * 
 */
public class Aplikacija {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Racunar asus = new Racunar(); // Ovde smo stavili breakpoint. U debug režimu izvršavane se zaustavlja ovde.
		asus.setProizvodjac("Asus");
		asus.setProcesor("Intel i9");
		// Izostavili smo podešavanje atributa napajanje, kako bismo u debug režimu pronašli da mu
		// je podrazumevana vrednost 0.
		asus.setRadniTaktProcesora(3.5f);
		asus.setGrafickaKarta("NVIDIA RTX2080");
		asus.setVodenoHladjenje(true);
		System.out.println(asus.specifikacija());
		System.out.println("Ukljucivanje racunara: " + asus.ukljuci(750)); // Uključivanje radi čak iako nije 
																		   // postavljena vrednost za napajanje 
																		   // (jer je podrazumevano 0).
		asus.iskljuci();
		// Instanciranje putem konstruktora sa parametrima, prosleđuju mu se argumenti.
		Racunar dell = new Racunar("Dell", "AMD Ryzen 7", 4.8f, 750, "AMD Radeon RX Vega 56", false);
		System.out.println(dell.specifikacija());
		System.out.println("Ukljucivanje racunara: " + dell.ukljuci(500));
		dell.iskljuci();
	}

}
