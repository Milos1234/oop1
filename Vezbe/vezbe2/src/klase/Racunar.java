/**
 * @author Aleksandra Mitrović
 * @date ned, 3. mart 2019.
 * vezbe2 / klase / Racunar.java
 * TODO
 */
package klase;

/**
 * 
 */
public class Racunar {
	// Sekcija atributa, najbolje je staviti ih na početku tela klase.
	private String proizvodjac;
	private String procesor;
	private float radniTaktProcesora;
	private int napajanje; // jacina napajanja
	private String grafickaKarta;
	private boolean vodenoHladjenje;
	
	
	//Potom staviti sve konstruktore klase
	// Konstruktor bez parametara
	public Racunar() {}

	/**
	 * Konstruktor sa parametrima. Ovo je komentar koji uključuje dokumentaciju ove metode.
	 * Komentar dokumentacije se započinje kosom crtom, potom dva znaka zvezdica (*), pa tekst koji
	 * se piše u svakom novom redu kojem prethodi znak zvezdica. Voditi računa o razmacima.
	 * Ako želte da naglasite da je nešto parametar metode dodaje se ATparam pa potom naziv tog parametra.
	 * @param proizvodjac
	 * @param procesor
	 * @param radniTaktProcesora
	 * @param napajanje
	 * @param grafickaKarta
	 * @param vodenoHladjenje
	 */
	public Racunar(String proizvodjac, String procesor, float radniTaktProcesora, int napajanje, String grafickaKarta,
			boolean vodenoHladjenje) {
		super();
		this.proizvodjac = proizvodjac;
		this.procesor = procesor;
		this.radniTaktProcesora = radniTaktProcesora;
		this.napajanje = napajanje;
		this.grafickaKarta = grafickaKarta;
		this.vodenoHladjenje = vodenoHladjenje;
	}

	// Nakon konstruktora staviti get i set metode u paru za dati atribut.
	public String getProizvodjac() {
		return proizvodjac;
	}

	public void setProizvodjac(String proizvodjac) {
		this.proizvodjac = proizvodjac;
	}

	public String getProcesor() {
		return procesor;
	}

	public void setProcesor(String procesor) {
		this.procesor = procesor;
	}

	public float getRadniTaktProcesora() {
		return radniTaktProcesora;
	}

	public void setRadniTaktProcesora(float radniTaktProcesora) {
		this.radniTaktProcesora = radniTaktProcesora;
	}

	public int getNapajanje() {
		return napajanje;
	}

	public void setNapajanje(int napajanje) {
		this.napajanje = napajanje;
	}

	public String getGrafickaKarta() {
		return grafickaKarta;
	}

	public void setGrafickaKarta(String grafickaKarta) {
		this.grafickaKarta = grafickaKarta;
	}

	// Voditi računa da je preporuka pisanje get metoda nad boolean metodama
	// zapravo u formatu isIMEPROMENLJIVE.
	public boolean isVodenoHladjenje() {
		return vodenoHladjenje;
	}

	public void setVodenoHladjenje(boolean vodenoHladjenje) {
		this.vodenoHladjenje = vodenoHladjenje;
	}
	
	// Nakong get-set metoda staviti sve specifične metode za datu klasu.
	public boolean ukljuci(int napajanje) {
		if (napajanje < this.napajanje) {
			return false;
		} else {
			return true;
		}
	}
	
	public void iskljuci() {
		System.out.println("Iskljucivanje racunara: " + procesor);
	}
	
	// Za formatiranje string pogledati dokumentaciju na:
	// https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Formatter.html
	// Pogledati tabelu sa znakovima za formatiranje.
	// Naći značenje za: %s, %f, %d, %b koji se koriste u narednom stringu.
	public String specifikacija() {
		return String.format("Proizvodjac: %s, procesor: %s, radni takt: %f, jacina napajanja: %d, graficka karta: %s, ima vodeno hladjenje: %b", 
				proizvodjac, procesor, radniTaktProcesora, napajanje, grafickaKarta, vodenoHladjenje);
	}
}
