/**
 * @author Aleksandra Mitrović
 * @date pon, 4. mart 2019.
 * vezbe2 / klase / Zadatak2.java
 * TODO
 */
package klase;

/**
 * Kreirati klasu Zadatak2 sa main metodom.
 * Kreirati novi String sa sadržajem: Univerzitet Singidunum.
 * Pronaći na kojoj se poziciji nalazi karakter S.
 * Pristupiti karakteru S iz datog stringa.
 * Kreirati podstring tako da sadrži Singidunum.
 * Pretvoriti početni String u niz karaktera.
 * 
 * TODO: Cilj je naučiti i primeniti metode klase String.
 * Dokumentacija: https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html
 */
public class Zadatak2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String novi = "Univerzitet Singidunum"; // string se piše samo pod dvostrukim navodnicima
		char s = 'S'; // karakter se piše pod jednostrukim navodinicima
		int pozicija = novi.indexOf(s); // pozicija karaktera S u stringu novi
		String podstring = novi.substring(pozicija, novi.length()); // kreira novi podstring koji od indeksa pozicija
																	// do kraja stringa, postoji i drugi način za
																	// korišćenje metode substring(), pogledati dokum.
		System.out.println(podstring); // radi kontrole ispisujemo isečeni string
		char[] nizKaraktera = novi.toCharArray();
		System.out.println(nizKaraktera[0]); // štampanje prvog karaktera iz niza radid provere da li je 'U'
	}

}
