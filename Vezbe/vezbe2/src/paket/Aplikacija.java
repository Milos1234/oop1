/**
 * @author Aleksandra Mitrović
 * @date pon, 4. mart 2019.
 * vezbe2 / paket / Aplikacija.java
 * TODO
 */
package paket;

import klase.Racunar;

/**
 * Kreirati novi paket sa nazivom paket.
 * U paketu definisati klasu Osoba koja ima dva privatna atributa tipa String: ime i prezime.
 * U klasi dodati konstruktore i get i set metode za atribute.
 * U paketu kreirati klasu Aplikacija sa main metodom.
 * Napraviti instancu Osobe i Računara iz prethodnog prvog zadatka.
 * Štampati podatke osobe pomoću get metoda, a podatke računara pomoću specifikacija metode.
 * TODO: Cilj je savladati importovanje klasa iz drugih paketa.
 */
public class Aplikacija {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Osoba osoba = new Osoba("Marko", "Marković");
		Racunar racunar = new Racunar(); // FIXME: prepraviti da se poziva konstruktor sa parametrima
		System.out.println("Osoba se zove: " + osoba.getIme() + " " + osoba.getPrezime());
		System.out.println(racunar.specifikacija());
	}

}
