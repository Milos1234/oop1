/**
 * @author Aleksandra Mitrović
 * @date pon, 4. mart 2019.
 * vezbe2 / paket / Osoba.java
 * TODO
 */
package paket;

public class Osoba {
	private String ime;
	private String prezime;
	
	public Osoba() {}
	
	public Osoba(String ime, String prezime) {
		super();
		this.ime = ime;
		this.prezime = prezime;
	}
	
	public String getIme() {
		return ime;
	}
	
	public void setIme(String ime) {
		this.ime = ime;
	}
	
	public String getPrezime() {
		return prezime;
	}
	
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
}
