/**
 * @author Aleksandra Mitrović
 * @date ned, 7. april 2019.
 * vezbe6 / vezbe / Zadatak6.java
 * TODO
 */
package vezbe;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;

/**
 * 
 */
public class Zadatak6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap<String, Float> sastojciCipsa = new HashMap<String, Float>();
		sastojciCipsa.put("Krompir", 90f);
		sastojciCipsa.put("So", 10f);

		HashMap<String, Float> sastojciFlipsa = new HashMap<String, Float>();
		sastojciFlipsa.put("Kukuruz", 90f);
		sastojciFlipsa.put("So", 5f);
		sastojciFlipsa.put("Kikiriki", 5f);

		Cips c1 = new Cips("Stark", sastojciCipsa, LocalDate.parse("2018-12-31"), LocalDate.parse("2019-01-30"),
				"89735431321", 56f);
		Cips c2 = new Cips("Marbo", sastojciCipsa, LocalDate.parse("2018-12-15"), LocalDate.parse("2019-05-03"),
				"89737411321", 48f);

		Flips f1 = new Flips("Jumbo", sastojciFlipsa, LocalDate.now(), LocalDate.parse("2019-10-15"), "64686410313",
				64f);
		Flips f2 = new Flips("Clipsy", sastojciFlipsa, LocalDate.now(), LocalDate.parse("2019-10-15"), "64686410313",
				28f);
		
		System.out.println(c1.isticeZa());
		
		HashSet<PrehrambeniProizvod> proizvodi = new HashSet<PrehrambeniProizvod>();
		proizvodi.add(c1);
		proizvodi.add(c2);
		proizvodi.add(f1);
		proizvodi.add(f2);
		
		PrehrambeniProizvod najjeftiniji = c1;
		for (PrehrambeniProizvod ph : proizvodi) {
			if (najjeftiniji.getCena() > ph.getCena()) {
				najjeftiniji = ph;
			}
		}
		
		System.out.println(najjeftiniji);

	}

}
