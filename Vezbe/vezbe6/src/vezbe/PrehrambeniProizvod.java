/**
 * @author Aleksandra Mitrović
 * @date ned, 7. april 2019.
 * vezbe6 / vezbe / PrehrambeniProizvod.java
 * TODO
 */
package vezbe;

import java.time.LocalDate;
import java.util.HashMap;

/**
 * 
 */
public abstract class PrehrambeniProizvod extends Proizvod {
	protected String barkod;
	protected float cena;
	
	public PrehrambeniProizvod() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PrehrambeniProizvod(String proizvodjac, HashMap<String, Float> sastav, LocalDate datumProizvodnje,
			LocalDate datumIstakaRoka, String barkod, float cena) {
		super(proizvodjac, sastav, datumProizvodnje, datumIstakaRoka);
		this.barkod = barkod;
		this.cena = cena;
	}

	public String getBarkod() {
		return barkod;
	}

	public void setBarkod(String barkod) {
		this.barkod = barkod;
	}

	public float getCena() {
		return cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}
	
}
