/**
 * @author Aleksandra Mitrović
 * @date ned, 7. april 2019.
 * vezbe6 / vezbe / Flips.java
 * TODO
 */
package vezbe;

import java.time.LocalDate;
import java.util.HashMap;

/**
 * 
 */
public class Flips extends PrehrambeniProizvod {

	public Flips() {
		super();
	}

	public Flips(String proizvodjac, HashMap<String, Float> sastav, LocalDate datumProizvodnje,
			LocalDate datumIstakaRoka, String barkod, float cena) {
		super(proizvodjac, sastav, datumProizvodnje, datumIstakaRoka, barkod, cena);
	}
	
}
