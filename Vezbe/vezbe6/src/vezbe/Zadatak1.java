/**
 * @author Aleksandra Mitrović
 * @date ned, 24. mart 2019.
 * vezbe5 / vezbe / Zadatak1.java
 * TODO
 */
package vezbe;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;

/**
 * 
 */
public class Zadatak1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	 	// TODO: Hashtable
		
		Hashtable<String, Integer> brojevi = new Hashtable<String, Integer>();
		/*
		 * Hashtable ne dozvoljava null vrednosti za kljuc ili vrednost, 
		 * tako da sledeca linija ce izazvati izuzetak.
		 */
		try {
			brojevi.put(null, null);
		} catch (NullPointerException e) {
			System.out.println("Nemoguce je dodati null vrednosti u hashtable!");
		}
		
		for (int i = 1; i <= 10; i++) {
			brojevi.put(String.format("%d", i), Integer.valueOf(i));
		}
		// Ispisivanjem se moze uociti da se ne cuva redosled dodatih elemenata.
		System.out.println(brojevi);
		
		for (Integer value : brojevi.values()) {
			System.out.println(value);
		}
		
		System.out.println("Kljuc 2 postoji? " + brojevi.containsKey("2"));
		System.out.println("Vrednost 15 postoji? " + brojevi.containsValue(Integer.valueOf(15)));
		
		
		while (!brojevi.isEmpty()) {
			System.out.println(brojevi.remove(brojevi.keySet().toArray()[0]));
		}
		System.out.println("Velicina hestabele: " + brojevi.size());
		
		// TODO: HashMap
		HashMap<String, String> gradoviDrzave = new HashMap<String, String>();
		/*
		 * Dodavanje null vrednosti u HashMap je dozvoljeno za razliku od Hashtable.
		 */
		gradoviDrzave.put(null, null);
		Scanner in = new Scanner(System.in);
		int counter = 3; // Za vise unosa promeniti brojac.
		while (counter > 0) {
			System.out.print("Unesite grad: ");
			String grad = in.nextLine();
			System.out.print("Unesite drzavu u kojoj je grad " + grad + ": ");
			String drzava = in.nextLine();
			gradoviDrzave.put(grad, drzava);
			counter--;
		}
		
		
		Collection<String> vrednosti = gradoviDrzave.values();
		Iterator<String> iter = vrednosti.iterator();
		while(iter.hasNext()) {
			System.out.println(iter.next()); // TODO: da li je ocuvan poredak dodati elemenata?
		}
		
		gradoviDrzave.clear();
		
		// TODO: HashSet
		HashSet<String> tekstovi = new HashSet<String>();
		
		counter = 5;
		while (counter > 0) {
			System.out.print("Unesite proizvoljan tekst: ");
			String tekst = in.nextLine();
			tekstovi.add(tekst);
			System.out.println("Broj karaktera u tekstu je: " + tekst.length());
			counter--;
		}
		in.close(); // zatvaranje skenera
		
		iter = tekstovi.iterator();
		boolean pronadjen = false;
		while (iter.hasNext()) {
			if (iter.next().equals("Singidunum")) {
				pronadjen = true;
				break;
			}
		}
		if (!pronadjen) {
			tekstovi.add("Singidunum");
		}
		// FIXME: za pronalazak postoji efikasniji nacin provere upotrebom contains metode nad skupom.
		System.out.println(tekstovi);
		// TODO: Proveriti da li ima ponavljanja u skupu (na osnovu unosa)?
	}

}
