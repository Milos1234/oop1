/**
 * @author Aleksandra Mitrović
 * @date ned, 7. april 2019.
 * vezbe6 / vezbe / Proizvod.java
 * TODO
 */
package vezbe;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Objects;

/**
 * Apstraktna klasa koja predstavlja bilo koji proizvod. Konkretizacije: -
 * PrehrambreniProizvod
 */
public abstract class Proizvod {
	protected String proizvodjac;
	protected HashMap<String, Float> sastav;
	protected LocalDate datumProizvodnje;
	protected LocalDate datumIstakaRoka;

	public Proizvod() {
		super();
	}

	public Proizvod(String proizvodjac, HashMap<String, Float> sastav, LocalDate datumProizvodnje,
			LocalDate datumIstakaRoka) {
		super();
		this.proizvodjac = proizvodjac;
		this.sastav = sastav;
		this.datumProizvodnje = datumProizvodnje;
		this.datumIstakaRoka = datumIstakaRoka;
	}

	public String getProizvodjac() {
		return proizvodjac;
	}

	public void setProizvodjac(String proizvodjac) {
		this.proizvodjac = proizvodjac;
	}

	public HashMap<String, Float> getSastav() {
		return sastav;
	}

	public void setSastav(HashMap<String, Float> sastav) {
		this.sastav = sastav;
	}

	public LocalDate getDatumProizvodnje() {
		return datumProizvodnje;
	}

	public void setDatumProizvodnje(LocalDate datumProizvodnje) {
		this.datumProizvodnje = datumProizvodnje;
	}

	public LocalDate getDatumIstakaRoka() {
		return datumIstakaRoka;
	}

	public void setDatumIstakaRoka(LocalDate datumIstakaRoka) {
		this.datumIstakaRoka = datumIstakaRoka;
	}

	@Override
	public String toString() {
		return "Proizvod [proizvodjac=" + proizvodjac + ", sastav=" + sastav + ", datumProizvodnje=" + datumProizvodnje
				+ ", datumIstakaRoka=" + datumIstakaRoka + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(datumIstakaRoka, datumProizvodnje, proizvodjac, sastav);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Proizvod other = (Proizvod) obj;
		return Objects.equals(datumIstakaRoka, other.datumIstakaRoka)
				&& Objects.equals(datumProizvodnje, other.datumProizvodnje)
				&& Objects.equals(proizvodjac, other.proizvodjac) && Objects.equals(sastav, other.sastav);
	}
	
	public long isticeZa() {
		return ChronoUnit.DAYS.between(datumProizvodnje, datumIstakaRoka);
	}
}
