/**
 * @author Aleksandra Mitrović
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Zadatak8.java
 * TODO
 */
package vezbe;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 
 */
public class Zadatak8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PrintWriter pw;
		try {
			pw = new PrintWriter("data/print.txt");
			pw.print(true);
			pw.print(20232.3);
			pw.print(333.0f);
			pw.print(1);
			pw.print('b');
			pw.print("String");
			pw.printf("%s %d", "Marko", 10);
			pw.close();

			FileReader fr = new FileReader("data/print.txt");
			int i;
			while ((i = fr.read()) != -1) {
				System.out.print((char) i);
			}
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
