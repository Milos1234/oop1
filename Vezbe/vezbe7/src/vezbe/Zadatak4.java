/**
 * @author Aleksandra Mitrović
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Zadatak4.java
 * TODO
 */
package vezbe;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 */
public class Zadatak4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BufferedInputStream bis;
		BufferedOutputStream bos;
		try {
			bis = new BufferedInputStream(new FileInputStream(new File("data/karakteri.txt")));
			bos = new BufferedOutputStream(new FileOutputStream(new File("data/karakteri-out.txt")));
			int sym;

			int mrk_pnt = 5;
			int rst_pnt = 10;

			for (int i = 1; i < 21; i++) {

				if (i == mrk_pnt) {
					bis.mark(0);
				}
				if (i == rst_pnt) {
					bis.reset();
				}

				sym = bis.read();
				System.out.print((char) sym);
				bos.write(sym);
				

			}
			bis.close();
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
