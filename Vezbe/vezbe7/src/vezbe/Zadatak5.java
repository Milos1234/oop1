/**
 * @author Aleksandra MitroviÄ‡
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Zadatak5.java
 * TODO
 */
package vezbe;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 */
public class Zadatak5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream("data/data-out.txt"));
			dos.writeBoolean(true);
			dos.writeByte(10);
			dos.writeChar('a');
			dos.writeDouble(0.1);
			dos.writeFloat(0.3f);
			dos.writeInt(5);
			dos.writeLong(133425464L);
			dos.writeUTF("Ä�Ä‡Å¾Å¡Ä‘");
			dos.close();
			
			DataInputStream dis = new DataInputStream(new FileInputStream("data/data-out.txt"));
			System.out.println(dis.readBoolean());
			System.out.println(dis.readByte());
			System.out.println(dis.readChar());
			System.out.println(dis.readDouble());
			System.out.println(dis.readFloat());
			System.out.println(dis.readInt());
			System.out.println(dis.readLong());
			System.out.println(dis.readUTF());
			dis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
