/**
 * @author Aleksandra Mitrović
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Tastatura.java
 * TODO
 */
package vezbe;

import java.io.Serializable;

/**
 * 
 */
public class Tastatura implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7011801894896015465L;
	int brojTastera;
	String raspored;
	boolean pozadinskoOsvetljenje;
	
	
	
	public Tastatura() {
		super();
	}

	public Tastatura(int brojTastera, String raspored, boolean pozadinskoOsvetljenje) {
		super();
		this.brojTastera = brojTastera;
		this.raspored = raspored;
		this.pozadinskoOsvetljenje = pozadinskoOsvetljenje;
	}

	@Override
	public String toString() {
		return "Tastatura [brojTastera=" + brojTastera + ", raspored=" + raspored + ", pozadinskoOsvetljenje="
				+ pozadinskoOsvetljenje + "]";
	}
}
