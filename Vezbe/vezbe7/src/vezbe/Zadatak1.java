/**
 * @author Aleksandra Mitrović
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Zadatak1.java
 * TODO
 */
package vezbe;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Calendar;

/**
 * 
 */
public class Zadatak1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File lipsum = new File("data/lorem ipsum.txt"); // putanja relativna u odnosu na Java projekat
		System.out.println(lipsum.getAbsolutePath());
		long time = lipsum.lastModified();
		
		// Kalendar konstruisemo uz pomoc staticke metode getInstance, a ne preko konstruktora.
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		LocalDate datum = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		System.out.println("Datum poslednje izmene: " + datum);
		System.out.println(lipsum.isFile());
		System.out.println(lipsum.isDirectory());

		// Kreiranje nove datoteke na zadatoj putanji (Zadatak 2)
		File nepostojeci = new File("data/nepostojeci.txt");
		try {
			System.out.println("Uspesno kreiran novi fajl: " + nepostojeci.createNewFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
