/*
 * \
++++lo;pk.................................................................................................... * @author Aleksandra Mitrović
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Zadatak6.java
 * TODO
 */
package vezbe;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 
 */
public class Zadatak6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Tastatura tastatura = new Tastatura(127, "srpski", false);
		System.out.println(tastatura);
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data/tastatura.txt"));
			oos.writeObject(tastatura);
			oos.close();
			
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data/tastatura.txt"));
			Tastatura ucitana = (Tastatura) ois.readObject();
			System.out.println("Ucitana:");
			System.out.println(ucitana);
			ois.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
