/**
0 * @author Aleksandra Mitrović
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Zadatak3.java
 * TODO
 */
package vezbe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 */
public class Zadatak3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileInputStream fis;
		FileOutputStream fos;
		try {
			fis = new FileInputStream(new File("data/singidunum-logo.png"));
			fis.readAllBytes();
			
			// FIXME: Kako smo dosli do kraja stream-a nemoguce je kreirati ovu 
			// kopiju slike. Onda, zakomentarisati 27 liniju pa pokusati ponovo!
			byte[] fisBytes;
			fisBytes = fis.readAllBytes();
			fis.close();
			for (byte b : fisBytes) {
				System.out.println(b);
			}
			fos = new FileOutputStream(new File("data/singidunum-logo-copy.png"));
			fos.write(fisBytes);
			fos.close();
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

}
