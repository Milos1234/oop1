/**
 * @author Aleksandra Mitrović
 * @date pon, 15. april 2019.
 * vezbe7 / vezbe / Zadatak7.java
 * TODO
 */
package vezbe;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 */
public class Zadatak7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			FileWriter fw = new FileWriter("data/dijakritici.txt");
			fw.write("Tekst koji testira karaktere: ћжђш i čćšđž");
			fw.close();
			
			FileReader fr = new FileReader("data/dijakritici.txt");
			int i;
		
			while((i = fr.read()) != -1) {
				System.out.print((char) i);
			}
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
