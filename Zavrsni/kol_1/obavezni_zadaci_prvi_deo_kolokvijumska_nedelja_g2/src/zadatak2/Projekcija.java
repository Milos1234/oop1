package zadatak2;

import zadatak1.Film;
import zadatak1.Zanr;

public class Projekcija extends Film {
	
	String opis;
	int trajanje;
	
	public Projekcija(String naziv, Zanr zanr, String opis, int trajanje) {
		super();
		super.naziv = naziv;
		super.zanr = zanr;
		this.opis = opis;
		this.trajanje = trajanje;
	}

	@Override
	public String toString() {
		return opis + " " + trajanje + " "+ super.toString();
	}

}
