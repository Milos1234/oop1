package zadatak4;

public class Test4 {

	public static void main(String[] args) {
		Pravougaonik prvi = new Pravougaonik();
		prvi.x = 10;
		prvi.y = 20;

		Pravougaonik drugi = new Pravougaonik();
		drugi.x = 20;
		drugi.y = 30;

		System.out.println("Povrsina prvog (10,20): " + prvi.povrsina());
		System.out.println("Povrsina drugog (20,30): " + drugi.povrsina());

		System.out.println(prvi.isPovrsina(drugi));
		System.out.println(drugi.isPovrsina(prvi));

	}

}
