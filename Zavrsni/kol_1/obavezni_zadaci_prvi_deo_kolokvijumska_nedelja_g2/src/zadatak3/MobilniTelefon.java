package zadatak3;

public class MobilniTelefon extends PrenosiviUredjaj {

	String operativniSistem;
	String model;

	public MobilniTelefon(String proizvodjac, double vrednost, String operativniSistem, String model) {
		super(proizvodjac, vrednost);
		this.operativniSistem = operativniSistem;
		this.model = model;
	}

	@Override
	public double prodajnaVrednost(double nabavnaVrednost) {
		if (nabavnaVrednost <= 100) {
			return nabavnaVrednost;
		} else {
			// Vrednost sa PDV-om
			return nabavnaVrednost + nabavnaVrednost * 20 / 100;
		}
	}

	@Override
	public boolean isProizvodjac(String p) {
		// TODO: Poredjenje instanci klasnih tipova se radi pomocu metode equals.
		// Ugradjeni Java tipovi vec imaju redefinisano znacenje ove metode.
		// Korisnici koji naprave svoje klase obicno redefinisu ovu metodu, kao i metodu hashCode.
		// TODO: Primitivni tipovi se porede sa ==, a takodje se porede i instance klasnih tipova kako bi se
		// proverilo da li su instance na istim referencama.
		if (super.proizvodjac.equals(p)) {
			return true;
		} else {
			return false;
		}
	}

}
