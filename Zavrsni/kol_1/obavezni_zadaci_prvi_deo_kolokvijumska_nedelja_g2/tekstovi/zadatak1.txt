Napisati nabrojivi tip enum Zanr koji sadrži sledeća polja: AKCIJA, DRAMA, TRILER, MJUZIKL, HOROR.

Napisati klasu Film koja sadrži sledeće: 
-Jedno polje naziv tipa String vidljivosti public i jedno polje zanr tipa Zanr, koje je vidljivosti protected.
-Konstruktor bez parametara. 
-Metode get i set za zanr.
-Metod oblika public String toString() koji vraća vrednosti polja (naslov i zanr) razdvojene razmakom. 

Napisati klasu Test1 koja pored metode main sadrži sledeće: 
-Inicijalizaciju objekata za dva filma, naziv prvog je “Ben-Hur” žanra DRAMA a naziv drugog je “La La Land” žanra MJUZIKL.
-Pomoću metode toString() ispisati vrednosti za oba objekta.