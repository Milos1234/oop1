Napisati klasu abstract class PrenosiviUredjaj koja sadrži sledeće: 
-Konstruktor sa parametrima i dva polja: proizvodjac tipa String i vrednost tipa double. 
-Metod oblika: abstract double prodajnaVrednost(double nabavnaVrednost).
-Metod oblika: abstract boolean isProizvodjac(String p).

Napisati klasu MobliniTelefon koja nasleđuje klasu PrenosiviUredjaj i sadrži sledeće: 
-Dodatno polje: operativni sistem (String operativniSistem) i model tipa String.
-Konstruktor oblika MobilniTelefon (String proizvodjac, double vrednost, String operativniSistem, String model).
-Redefinisati (@Override) metode prodajnaVrednost(double nabavnaVrednost) koji ako je nabavna vrednost veća od 100 
dodaje 20% PDV-a na tu vrednost i vraća je, ako je manja ili jednaka 100 vraća nabavnu vrednost, i isProizvodjac(String p) 
koji vraća true ako je vrednost proizvođača ista kao objekta p, inače false.

Napisati klasu Test3 koja pored metode main sadrži sledeće: 
-Inicijalizaciju dva objekta klase MobilniTelefon.
-Testirati metode prodajnaVrednost(double nabavnaVrednost) tako da se jednom dobije uneta nabavna vrednost, a jednom 
vrednost uvećana za PDV, i isProizvodjac(String p) tako da se jednom dobije true, a jednom false. Ispisati rezultate 
poziva metode na konzolu.