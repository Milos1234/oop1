package zadatak3;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*
 * Zadatak3
 * Napisati program koji sadrži sledeće:
 * -Inicijalizaciju skupa str  tipa Set<String> 
 * -Pomoću metode add skupu str dodati sledeće elemente: “ jedan”;  “dva  ” ; “   tri21   “;
 * -Kreirati iterator itr za skup str.
 * -Pomoću while petlje i metode itr.hasNext() formirati novi skup strTrim koji sadrži elemente skupa str u kojima su izostavljeni razmaci na početku i na kraju tih elemenata.  
 * -Štampati skupove str i strTrim.
 */
public class Zadatak3 {

	public static void main(String[] args) {
		Set<String> str = new HashSet<String>();
		
		str.add(" jedan");
		str.add("dva  ");
		str.add("   tri21   ");
		
		Iterator<String> itr = str.iterator();
		
		Set<String> strTrim = new HashSet<String>();
		while(itr.hasNext()) {
			strTrim.add(itr.next().trim());
		}
		
		System.out.println(str);
		System.out.println(strTrim);

	}

}
