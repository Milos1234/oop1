package zadatak1;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * Zadatak1 
 * Napisati program koji sadrži sledeće:
 * -Inicijalizaciju liste str tipa ArrayList <String> 
 * -Za promenljivu n uneti sa konzole pozitivan ceo broj i dodati obradu izuzetaka. 
 * -Pomoću naredbe for sa konzole dodati n elemenata u listu str.
 * -Pomoću naredbe for formirati listu strDodatak čiji elementi se dobijaju tako što se elementima liste str doda string “-dodato”.
 * -Ispisati liste str i strDodatak.
 */
public class Zadatak1 {

	public static void main(String[] args) {
		ArrayList<String> str = new ArrayList<String>();
		int n;
		Scanner scanner = new Scanner(System.in);
		while (true) {
			try {
				System.out.println("Unesite pozitivan ceo broj: ");
				n = scanner.nextInt();

				if (n < 0) {
					System.out.println("Morate uneti pozitivan broj!");
					scanner.nextLine(); // uklanjamo zaostali "\n" iz unosa broja
					continue;
				} else {
					scanner.nextLine(); // uklanjamo zaostali "\n" iz unosa broja
					break;
				}
			} catch (InputMismatchException e) {
				System.out.println("Niste uneli broj!");
				scanner.nextLine(); // uklanjamo zaostali "\n" iz unosa broja
			}

		}

		for (int i = 0; i < n; i++) {
			System.out.println("Unesite zeljeni string:");
			String unos = scanner.nextLine();
			str.add(unos);
		}
		scanner.close();

		ArrayList<String> strDodato = new ArrayList<String>();
		for (int i = 0; i < str.size(); i++) {
			strDodato.add(str.get(i) + "-dodato");
		}

		System.out.println(str);
		System.out.println(strDodato);

	}

}
