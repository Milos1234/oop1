package zadatak4;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * Koristeći tekstualni tok tipa FileReader i FileWriter napisati program koji sadrži sledeće:
 * -double d = 231.5e-2d;
 * -String smer = "SII"; 
 * -Upisati u datoteku dat1.txt  promenljive d i smer.
 * -Sadržaj datoteke dat1.txt prepisati u datoteku dat2.txt i ispisati sadržaj. 
 */
public class Zadatak4 {

	public static void main(String[] args) {
		try {
			FileWriter pisac = new FileWriter(new File("data/dat1.txt"));
			
			double d = 231.5e-2d;
			String smer = "SII"; 
			
			pisac.write(Double.toString(d));
			pisac.write(smer);
			pisac.close();
			
			FileReader citac = new FileReader(new File("data/dat1.txt"));
			pisac = new FileWriter(new File("data/dat2.txt"));
			int c;
			while((c = citac.read()) != -1) {
				pisac.write(c);
			}
			
			citac.close();
			pisac.close();
		}
		catch(IOException ex) {
			System.out.println("Greska pri radu sa datotekama");
		}

	}

}
