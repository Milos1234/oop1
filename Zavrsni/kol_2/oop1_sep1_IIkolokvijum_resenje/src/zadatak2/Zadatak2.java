package zadatak2;

import java.util.HashMap;

import org.json.JSONObject;

/*
 * Zadatak2
 * Napisati program koji sadrži sledeće:
 * -Inicijalizaciju mape auto  tipa HashMap<String, Integer> gde je ključ model auta marke AUDI a vrednost je cena modela u evrima. 
 * -Dodati promenljivu autoJson tipa JSONObject i pomoću metode accumulate dodati sledeće elemente: ("A3 1.8 TFSI", 24900), ("A3 2.0 TDI", 25400), ("A4 2.0 TDI", 30500), ("A6 2.0 TDI", 39800).
 * -Na osnovu promenljive autoJson  formirati promenljivu mapu auto.
 * -Koristeći programsku petlju formirati novu mapu autoPopust koja sadrži sve elemente iz mape auto sa cenom umanjenom za 10%. 
 *  Mapu autoPopust pretvoriti u JSON format i štampati. 
 */
public class Zadatak2 {

	public static void main(String[] args) {
		HashMap<String, Integer> auto = new HashMap<String, Integer>();
		JSONObject autoJson = new JSONObject();
		autoJson.accumulate("A3 1.8 TFSI", 24900);
		autoJson.accumulate("A3 2.0 TDI", 25400);
		autoJson.accumulate("A4 2.0 TDI", 30500);
		autoJson.accumulate("A6 2.0 TDI", 39800);
		
		for (String key : autoJson.keySet()) {
			auto.put(key, autoJson.getInt(key));
		}
		
		HashMap<String, Integer> autoPopust = new HashMap<String, Integer>();
		
		for (String key : auto.keySet()) {
			int vrednost = auto.get(key) - auto.get(key)*10/100; // originalna vrednost - (originalna vrednost * 10%)
			autoPopust.put(key, vrednost);
		}
		
		JSONObject autoPopustJson = new JSONObject(autoPopust);
		
		System.out.println(autoPopustJson);

	}

}
