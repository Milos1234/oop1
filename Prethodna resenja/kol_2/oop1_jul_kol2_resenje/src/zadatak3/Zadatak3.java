package zadatak3;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Zadatak3 {
	public static void main(String[] args) {
		Set<Integer> brojevi = new HashSet<Integer>();

		brojevi.add(21);
		brojevi.add(71);
		brojevi.add(-89);
		brojevi.add(33);

		Set<Float> brojeviPolovina = new HashSet<Float>();

		Iterator<Integer> itr = brojevi.iterator();
		while (itr.hasNext()) {
			brojeviPolovina.add(itr.next() / 2f);
		}

		System.out.println(brojevi);
		System.out.println(brojeviPolovina);
	}
}
