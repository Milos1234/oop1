package zadatak2;

import java.util.HashMap;

import org.json.JSONObject;

public class Zadatak2 {
	public static boolean isOsoba(Integer vrednost, HashMap<String, Integer> mapa) {
		if (mapa.containsValue(vrednost)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		HashMap<String, Integer> osobe = new HashMap<String, Integer>();
		JSONObject osobeJSON = new JSONObject();
		
		osobeJSON.accumulate("Ivan Ivanić", 123456);
		osobeJSON.accumulate("Marko Marić", 135670);
		osobeJSON.accumulate("Petar Perić", 981254);
		osobeJSON.accumulate("Mila Milić", 764980);
		
		for (String key : osobeJSON.keySet()) {
			osobe.put(key, osobeJSON.getInt(key));
		}
		
		System.out.println("Da li postoji vrednost 123456?" + isOsoba(123456, osobe));
	}
}
