package zadatak1;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadatak1 {

	public static void main(String[] args) {
		ArrayList<String> str = new ArrayList<String>();
		ArrayList<String> strVelikaSlova = new ArrayList<String>();
		Scanner in = new Scanner(System.in);
		String brojElemenata;

		try {
			System.out.println("Unesite zeljeni broj elemenata: ");
			brojElemenata = in.nextLine();
			int br = Integer.parseInt(brojElemenata);

			if (br < 0) {
				System.out.println("Broj je negativan!");
			} else {
				for (int i = 1; i <= br; i++) {
					String unos;
					System.out.println("Unesite neki string: ");
					unos = in.nextLine();
					str.add(unos);
				}
			}

		} catch (NumberFormatException e) {
			System.out.println("Niste uneli broj!");
		}
		in.close();
		for (String s : str) {
			strVelikaSlova.add(s.toUpperCase());
		}

		System.out.println(str);
		System.out.println(strVelikaSlova);
	}

}
