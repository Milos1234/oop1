package zadatak4;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


// Autor: 2018/270676
public class Zadatak4 {

	public static void main(String[] args) {
		float broj = 231.5f;
		String str = "Ovo je string";
		

		try {
			FileWriter fw = new FileWriter(new File("dat1.txt"));
			
			fw.write(broj + "");
			fw.write(str);
			
			System.out.println("Upis zavrsen");
			
			fw.close();
			
		} catch (IOException e) {
			System.out.println("Greska...");
			e.printStackTrace();
		}
		
		try {
			FileReader fr2 = new FileReader(new File("dat1.txt"));
			FileWriter fw2 = new FileWriter(new File("dat2.txt"));
			
			int c;
			
			while((c = fr2.read())!= -1) {
				fw2.write(c);
			}
			System.out.println("Prepis zavrsen");
			
			fr2.close();
			fw2.close();
			
		} catch (IOException e) {
			System.out.println("Greska...");
			e.printStackTrace();
		}

	}

}
