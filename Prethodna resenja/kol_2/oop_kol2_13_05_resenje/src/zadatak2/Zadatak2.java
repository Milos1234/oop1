package zadatak2;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONObject;


// Autor: 2018/271634
public class Zadatak2 {

	public static void main(String[] args) {
		HashMap<String, Integer> studenti = new HashMap<String, Integer>();

		JSONObject studentiJson = new JSONObject();
		studentiJson.accumulate("1112", 5);
		studentiJson.accumulate("1113", 2);
		studentiJson.accumulate("1114", 6);
		studentiJson.accumulate("1115", 0);
		studentiJson.accumulate("1116", 1);

		Iterator<String> sutudentiJsonKeys = studentiJson.keys();
		while (sutudentiJsonKeys.hasNext()) {
			String key = sutudentiJsonKeys.next();
			studenti.put(key, studentiJson.getInt(key));
		}

		HashMap<String, Integer> studentiViseOd2 = new HashMap<String, Integer>();
		for (String studentKey : studenti.keySet()) {
			int vrednost = studenti.get(studentKey);
			if (vrednost > 2) {
				studentiViseOd2.put(studentKey, vrednost);
			}
		}

		JSONObject studentiViseOd2Json = new JSONObject(studentiViseOd2);
		System.out.println(studentiViseOd2Json);

	}

}
