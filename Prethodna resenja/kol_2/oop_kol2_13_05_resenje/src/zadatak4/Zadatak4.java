package zadatak4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

//Autor: 2018/271634
public class Zadatak4 {

	public static void main(String[] args) {
		try {
			FileOutputStream streamout = new FileOutputStream(new File("data/dat1.txt"));

			byte[] byteNiz = { '?', 'w', '1', '*', 'U', 'Q', '4', '!', 't', 'o', '#', 'i' };
			String unos = "Ovo su obavezni zadaci � drugi deo";

			streamout.write(byteNiz);
			streamout.write(unos.getBytes());
			streamout.close();

			FileInputStream streamin = new FileInputStream(new File("data/dat1.txt"));
			streamout = new FileOutputStream(new File("data/dat2.txt"));

			for (byte b : streamin.readAllBytes()) {
				streamout.write(b);
			}

			streamin.close();
			streamout.close();
			System.out.println("Odradjeno.");
		} catch (IOException ex) {
			System.out.println("Greska: " + ex);
		}

	}

}
