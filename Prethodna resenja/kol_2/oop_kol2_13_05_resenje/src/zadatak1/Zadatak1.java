package zadatak1;

import java.util.ArrayList;
import java.util.Scanner;


// Autor: 2018/271634
public class Zadatak1 {

	public static void main(String[] args) {
		ArrayList<Double> listaBrojeva = new ArrayList<Double>();

		Scanner scanner = new Scanner(System.in);
		int duzinaListe;
		while (true) {
			try {
				System.out.print("Unesite duzinu liste: ");
				duzinaListe = scanner.nextInt();
				scanner.nextLine();
				break;
			} catch (Exception ex) {
				System.out.println("Greska: " + ex);
				scanner.nextLine();			
			}
		}

		for (int i = 0; i < duzinaListe; i++) {
			try {
				System.out.print(String.format("Unesite broj %d: ", i + 1));
				double broj = scanner.nextDouble();
				listaBrojeva.add(broj);
				scanner.nextLine();
			} catch (Exception ex) {
				System.out.println("Greska: " + ex);
				scanner.nextLine();
				i--;
			}
		}

		scanner.close();

		ArrayList<Double> listaFunkcija = new ArrayList<Double>();
		for (int i = 0; i < duzinaListe; i++) {
			double x = listaBrojeva.get(i);
			listaFunkcija.add((x * x) - (x + 22));
		}

		System.out.println("Lista brojeva: " + listaBrojeva);
		System.out.println("Lista funkcija: " + listaFunkcija);

	}

}
