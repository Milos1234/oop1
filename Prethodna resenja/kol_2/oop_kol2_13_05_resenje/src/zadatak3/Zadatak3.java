package zadatak3;

import java.util.HashSet;
import java.util.Set;

// Autor: 2018/271634
public class Zadatak3 {

	public static void main(String[] args) {
		Set<Integer> brojevi = new HashSet<Integer>();
		brojevi.add(4);
		brojevi.add(-2);
		brojevi.add(65);
		brojevi.add(8);
		brojevi.add(-12);

		Set<Integer> brojeviPozitivni = new HashSet<Integer>();
		Set<Integer> brojeviNegativni = new HashSet<Integer>();

		for (int broj : brojevi) {
			if (broj > -1)
				brojeviPozitivni.add(broj);
			else
				brojeviNegativni.add(broj);
		}

		System.out.println("Brojevi: " + brojevi);
		System.out.println("Brojevi pozitivni: " + brojeviPozitivni);
		System.out.println("Brojevi negativni: " + brojeviNegativni);
	}

}
