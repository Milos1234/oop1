package zadatak3;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

// @author 2018/270243
public class Zadatak {

	public static void main(String[] args) {
		
		Set<Double> brojevi = new HashSet<>();
		
		brojevi.add(-11.0);
		brojevi.add(22.0);
		brojevi.add(33.0);
		brojevi.add(-44.0);
		
		Set<Double> veciOdNule = new HashSet<>();
		
		Iterator<Double> itr = brojevi.iterator();
		
		while(itr.hasNext()) {
			double sledeci = itr.next();
			if(sledeci > 0)
				veciOdNule.add(sledeci);
			
		}
		
		System.out.printf("Skup brojevi: %s", brojevi);
		System.out.printf("\nSkup veciOdNule: %s", veciOdNule);
		
	}

}
