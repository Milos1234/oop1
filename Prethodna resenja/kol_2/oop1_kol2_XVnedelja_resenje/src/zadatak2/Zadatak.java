package zadatak2;

import java.util.HashMap;

import org.json.JSONObject;

// @author 2018/270243
public class Zadatak {

	public static void main(String[] args) {
		
		HashMap<String, Integer> student = new HashMap<>();
		JSONObject studentJson = new JSONObject();
		
		studentJson.accumulate("2018/314", 1);
		studentJson.accumulate("2017/304", 8);
		studentJson.accumulate("2016/123", 5);
		studentJson.accumulate("2018/704", 2);
		studentJson.accumulate("2018/978", 10);
		
		for(String kljuc : studentJson.keySet()) {
			student.put(kljuc, studentJson.getInt(kljuc));
		}
		
		HashMap<String, Integer> viseOdDva = new HashMap<>();
		
		for(String kljuc : student.keySet()) {
			if(student.get(kljuc) > 2)
				viseOdDva.put(kljuc, student.get(kljuc));
		}
		
		JSONObject viseOdDvaJson = new JSONObject(viseOdDva);
		
		System.out.printf("Mapa viseOdDva: %s", viseOdDvaJson.toString(4));
		
	}

}
