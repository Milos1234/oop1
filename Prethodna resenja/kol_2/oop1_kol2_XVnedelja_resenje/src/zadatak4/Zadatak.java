package zadatak4;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

// @author 2018/270243
public class Zadatak {

	public static void main(String[] args) {
		
		FileReader fr;
		FileWriter fw1;
		FileWriter fw2;
		
		int x = 123;
		String student = "Marko Markovic";
		
		try {
			fw1 = new FileWriter(new File("data/dat1.txt"));
			
			fw1.write(String.valueOf(x));
			fw1.write(student);
			fw1.close();
			
			fw2 = new FileWriter(new File("data/dat2.txt"));
			fr = new FileReader(new File("data/dat1.txt"));
			int procitano;
			
			while((procitano = fr.read()) != -1) {
				fw2.write(procitano);
				System.out.print((char) procitano);
			}
			fw2.close();
			fr.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
