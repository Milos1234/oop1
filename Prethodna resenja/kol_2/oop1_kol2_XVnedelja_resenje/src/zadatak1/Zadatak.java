package zadatak1;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

// @author 2018/270243
public class Zadatak {

	public static void main(String[] args) {
		
		ArrayList<Integer> brojevi = new ArrayList<>();
		Scanner in = new Scanner(System.in);
		
		while(true) {
			int broj;
			brojevi.clear();
			try {
				for(int i = 0; i < 3; i++) {
					System.out.print("Unesite ceo broj >> ");
					broj = in.nextInt();
					in.nextLine();
					brojevi.add(broj);
				}
				in.close();
				break;
			} catch(InputMismatchException e) {
				in.nextLine();
				System.out.println("\nMorate uneti ceo broj!\n");
				continue;
			}
		}
		
		ArrayList<Integer> kvadratiBrojeva = new ArrayList<>();
		
		for(int i : brojevi) {
			kvadratiBrojeva.add(i * i);
		}
		
		System.out.printf("\nLista brojevi: %s", brojevi);
		System.out.printf("\nLista kvadratiBrojeva: %s", kvadratiBrojeva);
		
	}

}
