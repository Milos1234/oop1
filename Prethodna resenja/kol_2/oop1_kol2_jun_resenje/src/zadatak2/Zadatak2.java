/**
 * 
 */
package zadatak2;

import java.util.HashMap;

import org.json.JSONObject;

/**
 * @author Aleksandra
 *
 */

/*
 * Napisati program koji sadrži sledeće: 
 * -Inicijalizaciju mape voce tipa HashMap <String, Float> gde je ključ naziv voća a vrednost ključa cena voća. 
 * -Dodati promenljivu voceJson tipa JSONObject koja u JSON formatu sadrži elemente:
 *  ("Avokado ", 170.); ("Banane", 110.); ("Borovnica", 190.); ("Jabuka", 80.);
 * -Na osnovu promenljive voceJson formirati promenljivu mapu voce. 
 * -Koristeći programsku petlju formirati novu mapu cenaVecaOd90 koja sadrži sve elemente
 *  iz mape voce u kojima je cena voća veća od 90 (devedeset). 
 * -Mapu cenaVecaOd90 pretvoriti u JSON format i štampati.
 */
public class Zadatak2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap<String, Float> voce = new HashMap<>();
		JSONObject voceJson = new JSONObject();
		voceJson.put("Avokado ", 170.);
		voceJson.put("Banane", 110.);
		voceJson.put("Borovnica", 190.);
		voceJson.put("Jabuka", 80.);
		for (Object o : voceJson.keySet()) {
			String x = (String) o;
			voce.put(x, voceJson.getFloat(x));
		}
		HashMap<String, Float> cenaVecaOd90 = new HashMap<>();
		for (String v : voce.keySet()) {
			if (voce.get(v) > 90) {
				cenaVecaOd90.put(v, voce.get(v));
			}
		}
		JSONObject cenaVecaOd90Json = new JSONObject(cenaVecaOd90);
		System.out.println(cenaVecaOd90Json);

	}

}
