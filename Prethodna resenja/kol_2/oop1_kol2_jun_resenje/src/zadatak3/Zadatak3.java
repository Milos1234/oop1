/**
 * 
 */
package zadatak3;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Aleksandra
 *
 */

/*
 * Napisati program koji sadrži sledeće: Inicijalizaciju skupa brojevi tipa Set<Double> 
 * -Pomoću metode add skupu brojevi dodati sledeće brojeve: 21.98;
 *  71.34; -89.22; 33.87; 
 * -Kreirati iterator itr za skup brojevi. 
 * -Pomoću while petlje i metode itr.hasNext() formirati skup celiBrojevi 
 *  tipa Set<Integer> tako da sadrži cele brojeve od elemenata skupa brojevi. 
 * -Štampati skupove brojevi i celiBrojevi.
 */
public class Zadatak3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Set<Double> brojevi = new HashSet<Double>();
		brojevi.add(21.98);
		brojevi.add(71.34);
		brojevi.add(-89.22);
		brojevi.add(33.87);
		Set<Integer> celiBrojevi = new HashSet<Integer>();
		Iterator<Double> itr = brojevi.iterator();
		while (itr.hasNext()) {
			Double x = itr.next();
			Integer i = x.intValue();
			celiBrojevi.add(i);
		}
		System.out.println(brojevi);
		System.out.println(celiBrojevi);

	}

}
