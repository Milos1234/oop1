/**
 * 
 */
package zadatak1;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Aleksandra
 */

/*
 * Napisati program koji sadrži sledeće: -Inicijalizaciju liste brojevi tipa
 *  ArrayList <Double> 
 * -Pomoću naredbe for uneti 4 (četiri) broja sa konzole i
 *  dodati ih u listu brojevi. Pomoću obrade izuzetaka obezbediti unos sve dok se
 *  ne unese ispravna vrednost broja. 
 * -Pomoću naredbe for formirati listu
 *  umanjenjeBrojeva čiji elementi liste brojevi su umanjeni za 22 (dvadesetdva).
 * -Ispisati liste brojevi i umanjenjeBrojeva.
 */
public class Zadatak1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<Double> brojevi = new ArrayList<>();
		Scanner in = new Scanner(System.in);
		for (int i = 0; i < 4; i++) {
			System.out.print("Unesite broj: ");
			try {
				Double broj = in.nextDouble();
				brojevi.add(broj);
			} catch (InputMismatchException e) {
				System.out.println("Niste uneli broj!");
				in.nextLine();
				i--;
			}
		}
		in.close();
		ArrayList<Double> umanjenjeBrojeva = new ArrayList<>();
		for (Double d : brojevi) {
			umanjenjeBrojeva.add(d - 22);
		}
		System.out.println(brojevi);
		System.out.println(umanjenjeBrojeva);

	}

}
