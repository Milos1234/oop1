/**
 * 
 */
package zadatak4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Aleksandra
 *
 */

/*
 * Koristeći binarni tok tipa FileInputStream i FileOutputStream napisati
 * program koji sadrži sledeće: 
 * -byte[] x = {'n', 'i', 'z', ' ', 'b', 'a', 'j', 't', 'o', 'v', 'a'}; 
 * -String y = "Junski ispitni rok 2019. godine"; 
 * -Upisati u datoteku dat1.txt promenljive x i y. 
 * -Sadržaj datoteke dat1.txt prepisati u datoteku dat2.txt i ispisati sadržaj.
 */
public class Zadatak4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		byte[] x = { 'n', 'i', 'z', ' ', 'b', 'a', 'j', 't', 'o', 'v', 'a' };
		String y = "Junski ispitni rok 2019. godine";
		try {
			// Prvo zapisujemo
			FileOutputStream fos = new FileOutputStream("data/dat1.txt");
			fos.write(x);
			fos.write(y.getBytes());
			fos.close();
			// Nakon toga otvaramo za čitanje, a drugu datoteku za pisanje
			FileInputStream fis = new FileInputStream("data/dat1.txt");
			FileOutputStream fos2 = new FileOutputStream("data/dat2.txt");
			int i;
			while ((i = fis.read()) != -1) {
				fos2.write(i);
				System.out.print((char) i);
			}
			fos2.close();
			fis.close();
		} catch (IOException e) {
			e.getStackTrace();
		}

	}

}
