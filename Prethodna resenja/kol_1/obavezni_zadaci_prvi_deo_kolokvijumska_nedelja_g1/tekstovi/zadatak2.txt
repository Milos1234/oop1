Data je klasa Knjiga opisana u prvom zadatku.

Napisati klasu Obrada koja nasleđuje klasu Knjiga i sadrži sledeće: 
-Dodatna polja isbn tipa String i brPrimeraka tipa int.
-Konstruktor oblika Obrada (String autor, String naslov, Zanr zanr, String isbn, int brPrimeraka)
-Metod oblika void ispis(int i) gde se za i=0 poziva metod ispis() iz klase Knjiga, inače se ispisuje 
autor, naslov, zanr, isbn i brPrimeraka odvojeno razmakom.

Napisati klasu Test2 koja pored metode main sadrži sledeće: 
-Inicijalizaciju objekta klase Obrada i za taj objekat pozvati metode ispis(0) i ispis(1), prikazati rezultat na konzoli.