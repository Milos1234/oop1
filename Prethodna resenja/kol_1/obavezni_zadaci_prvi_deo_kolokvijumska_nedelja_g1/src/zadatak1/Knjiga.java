package zadatak1;

public class Knjiga {
	public String autor;
	public String naslov;
	protected Zanr zanr;
	
	public Knjiga() {
		super();
	}

	public Knjiga(String autor, String naslov, Zanr zanr) {
		super();
		this.autor = autor;
		this.naslov = naslov;
		this.zanr = zanr;
	}

	public Zanr getZanr() {
		return zanr;
	}

	public void setZanr(Zanr zanr) {
		this.zanr = zanr;
	}

	public String ispis() {
		// Formatiranje stringa konkatenacijom (spajanjem).
		return autor + " " + naslov + " " + zanr;
	}
}
