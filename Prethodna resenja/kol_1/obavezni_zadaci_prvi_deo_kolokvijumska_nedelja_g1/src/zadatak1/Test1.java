package zadatak1;

public class Test1 {

	public static void main(String[] args) {
		Knjiga prva = new Knjiga("Marko Markovic", "Objektno - orijentisani principi", Zanr.BIOGRAFIJA);
		Knjiga druga = new Knjiga("Janko Jankovic", "Uvod u java programiranje", Zanr.POEZIJA);
		Knjiga treca = new Knjiga();
		treca.autor = "Petar Petrovic";
		treca.naslov = "Kako postati programer?";
		treca.zanr = Zanr.KOMEDIJA;

		// TODO: Drugi nacin za postavljanje zanra je preko set metode
//		treca.setZanr(Zanr.KOMEDIJA);
		System.out.println(prva.ispis());
		System.out.println(druga.ispis());
		System.out.println(treca.ispis());
	}

}
