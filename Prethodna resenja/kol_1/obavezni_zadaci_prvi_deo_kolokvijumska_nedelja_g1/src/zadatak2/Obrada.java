package zadatak2;

import zadatak1.Knjiga;
import zadatak1.Zanr;

public class Obrada extends Knjiga {
	
	String isbn;
	int brPrimeraka;

	public Obrada(String autor, String naslov, Zanr zanr, String isbn, int brPrimeraka) {
		super(autor, naslov, zanr);
		this.isbn = isbn;
		this.brPrimeraka = brPrimeraka;
	}
	
	
	public void ispis(int i) {
		if (i == 0) {
			System.out.println(super.ispis());
			// TODO: Moguce je pozvati ovu metodu i bez super, jer postoji tacno jedna metoda
			// ispis koja nema argumenata, tako da je nedvosmisleno pozivanje metode.
//			System.out.println(ispis());
		} else {
			// Formatiranje stringa uz pomoc staticke metode format iz klase String.
			System.out.println(String.format("%s %s %s %s %d", autor, naslov, zanr, isbn, brPrimeraka));
		}
	}
}
