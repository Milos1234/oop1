package zadatak4;

public class Test4 {

	public static void main(String[] args) {
		Predmet prvi = new Predmet();
		prvi.naziv = "Objektno - orijentisano programiranje";
		prvi.poeni = 100;
		prvi.ocena = 10;

		Predmet drugi = new Predmet();
		drugi.naziv = "Osnove programiranja";
		drugi.poeni = 51;
		drugi.ocena = 6;

		System.out.println(drugi.isUporediPoene(prvi));
		System.out.println(prvi.isJednakeOcene(drugi));

	}

}
