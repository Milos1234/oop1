package zadatak4;

public class Predmet implements Uporedjivanje {
	String naziv;
	int poeni;
	int ocena;

	@Override
	public boolean isUporediPoene(Predmet p) {
		if (poeni < p.poeni) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isJednakeOcene(Predmet p) {
		if (ocena == p.ocena) {
			return true;
		} else {
			return false;
		}
	}

}
