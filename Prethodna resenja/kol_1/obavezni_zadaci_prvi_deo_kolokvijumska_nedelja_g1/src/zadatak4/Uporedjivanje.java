package zadatak4;

public interface Uporedjivanje {
	public boolean isUporediPoene(Predmet p);
	public boolean isJednakeOcene(Predmet p);
}
