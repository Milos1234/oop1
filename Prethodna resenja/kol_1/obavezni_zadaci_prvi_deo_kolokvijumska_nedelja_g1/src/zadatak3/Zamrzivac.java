package zadatak3;

public class Zamrzivac extends RashladniUredjaj {

	String model;
	int zapremina;

	public Zamrzivac(String proizvodjac, double minTemperatura, double maxTemperatura, String model, int zapremina) {
		super(proizvodjac, minTemperatura, maxTemperatura);
		this.model = model;
		this.zapremina = zapremina;
	}

	@Override
	protected int odnosTemperature() {
		if (minTemperatura == maxTemperatura) {
			return 0;
		} else if (minTemperatura > maxTemperatura) {
			return -1;
		} else {
			return 1;
		}
	}

}
