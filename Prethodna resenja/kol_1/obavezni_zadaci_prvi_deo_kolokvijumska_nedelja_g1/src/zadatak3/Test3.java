package zadatak3;

public class Test3 {

	public static void main(String[] args) {
		Zamrzivac prvi = new Zamrzivac("Gorenje", -10, 15, "Kombinovani", 100);
		Zamrzivac drugi = new Zamrzivac("Beko", 5, 5, "Obican", 20);
		Zamrzivac treci = new Zamrzivac("Vox", 20, 10, "Rucni", 5);

		System.out.println(prvi.odnosTemperature());
		System.out.println(drugi.odnosTemperature());
		System.out.println(treci.odnosTemperature());
	}

}
