package zadatak3;

public abstract class RashladniUredjaj {
	String proizvodjac;
	double minTemperatura;
	double maxTemperatura;
	
	// Konstruktor koriste samo njegove naslednice pa je shodno tome protected.
	protected RashladniUredjaj(String proizvodjac, double minTemperatura, double maxTemperatura) {
		super();
		this.proizvodjac = proizvodjac;
		this.minTemperatura = minTemperatura;
		this.maxTemperatura = maxTemperatura;
	}
	
	protected abstract int odnosTemperature();
}
