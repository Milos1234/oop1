package zadatak2;

import zadatak1.Zanr;

public class Test2 {

	public static void main(String[] args) {
		Projekcija projekcija = new Projekcija("Poslednji programer", Zanr.HOROR, "Poslednji programer je uzbudljivi "
				+ "film koji prikazuje kako izgleda svet kada poslednji programer nestane.", 120);
		
		System.out.println(projekcija);

	}

}
