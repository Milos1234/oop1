package zadatak3;

public abstract class PrenosiviUredjaj {
	String proizvodjac;
	double vrednost;
	
	protected PrenosiviUredjaj(String proizvodjac, double vrednost) {
		super();
		this.proizvodjac = proizvodjac;
		this.vrednost = vrednost;
	}
	
	public abstract double prodajnaVrednost(double nabavnaVrednost);
	public abstract boolean isProizvodjac(String p);
}
