package zadatak3;

public class Test3 {

	public static void main(String[] args) {
		MobilniTelefon prvi = new MobilniTelefon("Nokia", 15248, "Android One", "3310");
		MobilniTelefon drugi = new MobilniTelefon("Samsung", 154564, "Android One", "Galaxy Fold");

		System.out.println(prvi.prodajnaVrednost(51));
		System.out.println(drugi.prodajnaVrednost(168489));

		System.out.println(prvi.isProizvodjac("Nokia"));
		System.out.println(drugi.isProizvodjac("Samsung Galaxy"));
	}

}
