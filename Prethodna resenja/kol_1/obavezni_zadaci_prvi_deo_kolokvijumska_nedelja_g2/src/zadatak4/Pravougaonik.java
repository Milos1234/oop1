package zadatak4;

public class Pravougaonik implements Oblik {

	double x; // sirina
	double y; // visina

	@Override
	public double povrsina() {
		// TODO Auto-generated method stub
		return x * y;
	}

	@Override
	public boolean isPovrsina(Pravougaonik p) {
		if (povrsina() < p.povrsina()) {
			return true;
		} else {
			return false;
		}
	}

}
