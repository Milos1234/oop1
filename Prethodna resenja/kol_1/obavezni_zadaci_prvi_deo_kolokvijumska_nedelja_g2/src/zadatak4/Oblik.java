package zadatak4;

public interface Oblik {
	public double povrsina();
	public boolean isPovrsina(Pravougaonik p);
}