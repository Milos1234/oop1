package zadatak1;

public class Test1 {

	public static void main(String[] args) {
		Film prvi = new Film();
		prvi.naziv = "Ben-Hur";
		prvi.setZanr(Zanr.DRAMA);

		Film drugi = new Film();
		drugi.naziv = "La La Land";
		drugi.setZanr(Zanr.MJUZIKL);

		System.out.println(prvi.toString());
		// TODO: Poziv metode toString nije obavezan, jer stampanje na konzolu
		// zahteva da se prosledi String, pa se samim tim implicitno poziva
		// nad objektom metoda toString.
		System.out.println(drugi);

	}

}
