package zadatak1;

public class Knjiga {
	public String autor;
	public String naslov;
	protected Zanr zanr;
	
	public Knjiga() {}
	
	public Knjiga(String autor, String naslov, Zanr zanr) {
		this.autor = autor;
		this.naslov = naslov;
		this.zanr = zanr;
	}
	public Zanr getZanr() {
		return zanr;
	}
	public void setZanr(Zanr z) {
		this.zanr = z;
	}
	
	public String ispis() {
		return autor + " " + naslov + " " + zanr;
	}

}
