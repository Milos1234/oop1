package zadatak1;

public class Film {
	public String naziv;
	protected Zanr zanr;
	
	public Film() {
		super();
	}

	public Zanr getZanr() {
		return zanr;
	}

	public void setZanr(Zanr zanr) {
		this.zanr = zanr;
	}

	@Override
	public String toString() {
		return naziv + " " + zanr;
	}
	
	
}
