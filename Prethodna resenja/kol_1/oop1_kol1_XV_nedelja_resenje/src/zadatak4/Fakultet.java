package zadatak4;

public class Fakultet implements Studiranje {
	String naziv;
	float godina;

	@Override
	public float prosekFakultet() {
		return godina;
	}

	@Override
	public String nazivFakultet() {
		return naziv;
	}

	static float srednjeFakulteti(Fakultet[] fakulteti) {
		float suma = 0;
		for (Fakultet fakultet : fakulteti) {
			suma += fakultet.godina;
		}
		return suma / fakulteti.length;
	}
}