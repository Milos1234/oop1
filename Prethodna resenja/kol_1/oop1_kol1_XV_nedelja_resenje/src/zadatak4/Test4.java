package zadatak4;

public class Test4 {
	public static void main(String[] args) {
		Fakultet prvi = new Fakultet();
		prvi.naziv = "Tehnicki fakultet";
		prvi.godina = 4;

		Fakultet drugi = new Fakultet();
		drugi.naziv = "Poslovna ekonomija";
		drugi.godina = 3;

		Fakultet treci = new Fakultet();
		treci.naziv = "Fakultet za turizam i hotelijerstvo";
		treci.godina = 5;

		System.out.println(prvi.prosekFakultet());
		System.out.println(prvi.nazivFakultet());

		Fakultet[] fakulteti = new Fakultet[3];
		fakulteti[0] = prvi;
		fakulteti[1] = drugi;
		fakulteti[2] = treci;

		System.out.println("Vrednost prosecnog broja studiranja(4, 3, 5) je: " + Fakultet.srednjeFakulteti(fakulteti));

	}
}