package zadatak3;

public class Registar extends Preduzece {
	double potrosnja;
	double zarada;

	public Registar(String naziv, String adresa, double potrosnja, double zarada) {
		super(naziv, adresa);
		this.potrosnja = potrosnja;
		this.zarada = zarada;
	}

	@Override
	void uspesnost() {
		if (zarada > potrosnja) {
			System.out.println("Preduzece je uspesno.");
		} else {
			System.out.println("Preduzece nije uspesno.");
		}
	}
}
