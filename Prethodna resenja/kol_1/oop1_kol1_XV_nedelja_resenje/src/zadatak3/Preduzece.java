package zadatak3;

public abstract class Preduzece {
	String naziv;
	String adresa;

	public Preduzece(String naziv, String adresa) {
		this.naziv = naziv;
		this.adresa = adresa;
	}

	abstract void uspesnost();

	void promenaNaziva(String noviNaziv) {
		this.naziv = noviNaziv;
	}
}
