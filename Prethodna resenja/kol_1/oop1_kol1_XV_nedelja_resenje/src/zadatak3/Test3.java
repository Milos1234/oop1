package zadatak3;

public class Test3 {
	public static void main(String[] args) {
		Registar prvi = new Registar("Moja radnja", "Bulevar Cara Lazara", 40000, 33000);
		Registar drugi = new Registar("Krofnica", "Cenejska", 65000, 74000);

		prvi.uspesnost();
		drugi.uspesnost();

		System.out.println(prvi.naziv);
		prvi.promenaNaziva("Igraonica Jumbo");
		System.out.println(prvi.naziv);
	}
}
