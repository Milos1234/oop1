package zadatak1;

public class Student {
	public String ime;
	public String prezime;

	public Student() {

	}

	public Student(String ime, String prezime) {
		this.ime = ime;
		this.prezime = prezime;
	}

	public void ispis(Zaposlenost z) {
		System.out.println("Ime: " + ime + ", Prezime: " + prezime + ", Zaposlenost: " + z);
	}
}
