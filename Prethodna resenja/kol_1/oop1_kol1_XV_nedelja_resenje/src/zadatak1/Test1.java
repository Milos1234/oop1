package zadatak1;

public class Test1 {
	public static void main(String[] args) {
		Student prvi = new Student("Marko", "Markovic");
		Student drugi = new Student();
		drugi.ime = "Janko";
		drugi.prezime = "Jankovic";

		System.out.println(prvi.ime);
		System.out.println(prvi.prezime);
		System.out.println(drugi.ime);
		System.out.println(drugi.prezime);

		prvi.ispis(Zaposlenost.ZAPOSLEN);
		drugi.ispis(Zaposlenost.NEZAPOSLEN);
	}
}
