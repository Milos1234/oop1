package zadatak2;

import zadatak1.Student;

public class StudentFakulteta extends Student {
	String indeks;

	public StudentFakulteta(String ime, String prezime, String indeks) {
		super(ime, prezime);
		this.indeks = indeks;
	}

	String podaci() {
		return ime + " " + prezime + " " + indeks;
	}
}
