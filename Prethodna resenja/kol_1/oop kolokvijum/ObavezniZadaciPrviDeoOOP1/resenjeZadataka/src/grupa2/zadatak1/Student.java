package grupa2.zadatak1;

public class Student extends Osoba {

	String indeks;
	
	public Student(int starost, String imePrezime, String indeks) {
		super(starost, imePrezime);
		this.indeks = indeks;
	}
	
	@Override
	public String predstaviSe() {
		return indeks + super.predstaviSe();
	}
	
	public int velikaSlova() {
		int pojava = 0;
		for (int i=0; i < imePrezime.length(); i++) {
			if (Character.isUpperCase(imePrezime.charAt(i))) {
				pojava++;
			}
		}
		return pojava;
	}
	
}
