package grupa1.zadatak3;

public class Student {
	public String ime;
	public String prezime;
	static String grupa;
	protected Smer smer;

	Student(String ime, String prezime,Smer Smer) {
		this.ime = ime;
		this.prezime = prezime;
		this.smer = smer;
	}
	
	public Smer getSmer() {
		return smer;
	}

	public void setSmer(Smer smer) {
		this.smer = smer;
	}

	void ispis() {
		System.out.println(ime + " " + prezime + " " + grupa);
	}
	
	public void ispisSmer() {
		System.out.format("%s %s %s", ime, prezime, smer);
	}
}
