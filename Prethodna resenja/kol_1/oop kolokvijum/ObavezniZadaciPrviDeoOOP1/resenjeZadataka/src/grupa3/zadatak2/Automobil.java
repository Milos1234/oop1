package grupa3.zadatak2;

public class Automobil extends Vozilo {
	Karoserija karoserija;

	public Automobil() {}

	public Automobil(Osoba vlasnik, String registarskaOznaka, String brojSasije, Karoserija karoserija) {
		super(vlasnik, registarskaOznaka, brojSasije);
		this.karoserija = karoserija;
	}

	@Override
	public void registruj(Osoba vlasnik) {
		int iznos = 0;
		
		//Moze se uraditi i preko if-a, ali mislim da je ovako jednostavnije
		switch (karoserija) {
		case HECBEK:
			iznos = 10000;
			break;
		case LIMUZINA:
			iznos = 15000;
			break;
		case KARAVAN:
			iznos = 20000;
			break;
		}
		
		if (this.vlasnik.equals(vlasnik)) {
			iznos -= 2000;
		} else {
			// Vlasnika menjamo samo u slucaju da se razlikuje od prethodnog.
			this.vlasnik = vlasnik;
		}
		
		// Moze se resiti i obicnom konkatenacijom stringova.
		System.out.format("Registracija za vlasnika %s %s, automobila registarskih oznaka: %s iznosi %d dinara.\n", vlasnik.ime, vlasnik.prezime, registarskaOznaka, iznos);
	}
}
