package grupa4.zadatak1;

public class SwissBanka implements Banka {
	String naziv;
	String adresa;

	public float kamata() {
		return 2.15f;
	}

	public String podaciBanka() {
		return "Naziv banke: " + naziv + "; Adresa: " + adresa;
	}
	
	public boolean sadrziBanku(SwissBanka[] banke) {
		for (SwissBanka banka : banke) {
			if (banka.naziv == naziv) {
				return true; // cim se naidje na prvu pojavu vraca se vrednost true
			}
		}
		return false;
	}
}
