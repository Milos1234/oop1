package grupa4.zadatak2;

public class RSBicikl implements Bicikl {
	int brzinaNaMenjacu;
	int brzina;
	
	@Override
	public void promenaBrzineNaMenjacu(int broj) {
		brzinaNaMenjacu = broj;

	}

	@Override
	public void ubrzaj(int povecajZa) {
		brzina = brzina + povecajZa;

	}

	@Override
	public void koci(int smanjiZa) {
		brzina = brzina - smanjiZa;

	}
	
	void printStanje() {
		System.out.println("Brzina je: " + brzina + "; Broj menjača: " + brzinaNaMenjacu);
	}
	
	public static float srednjaBrzina(RSBicikl[] bicikli) {
		float suma = 0;
		for (RSBicikl bicikl : bicikli) {
			suma += bicikl.brzina;
		}
		return suma / bicikli.length;
	}

}
