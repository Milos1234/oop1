package grupa4.zadatak2;

public class Test42 {

	public static void main(String[] args) {
		RSBicikl rsb = new RSBicikl();
		rsb.promenaBrzineNaMenjacu(2);
		rsb.ubrzaj(5);
		rsb.printStanje();
		RSBicikl drugi = new RSBicikl();
		drugi.ubrzaj(5);
		RSBicikl novi = new RSBicikl();
		novi.ubrzaj(10);
		RSBicikl[] bicikli = new RSBicikl[3];
		bicikli[0] = rsb;
		bicikli[1] = drugi;
		bicikli[2] = novi;
		System.out.println("Prosecna brzina bicikala (5, 5, 10) je: " + RSBicikl.srednjaBrzina(bicikli));
	}

}
