package zadatak1;

public class Knjiga {
	public String isbn;
	protected Klasifikacija klasifikacija;
	
	
	public Knjiga() {
		super();
	}

	public Knjiga(String isbn, Klasifikacija klasifikacija) {
		super();
		this.isbn = isbn;
		this.klasifikacija = klasifikacija;
	}

	public Klasifikacija getKlasifikacija() {
		return klasifikacija;
	}

	public void setKlasifikacija(Klasifikacija klasifikacija) {
		this.klasifikacija = klasifikacija;
	}
	
	public String ispis() {
		return isbn + " " + klasifikacija;
	}
	
}
