package zadatak1;

public class Test1 {

	public static void main(String[] args) {
		Knjiga k1 = new Knjiga("9485-56564-21-4-156", Klasifikacija.INFORMATIKA);
		Knjiga k2 = new Knjiga("16685-2355-8-635-78", Klasifikacija.INFORMATIKA);
		
		Knjiga k3 = new Knjiga();
		k3.isbn = "48-4665-61587-661-4";
		k3.setKlasifikacija(Klasifikacija.KNJIŽEVNOST);
		
		System.out.println(k1.ispis());
		System.out.println(k2.ispis());
		System.out.println(k3.ispis());

	}

}
