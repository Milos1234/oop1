Zadatak2
Data je klasa Knjiga opisana u prvom zadatku.  
Napisati klasu Katalogizacija koja nasleđuje klasu Knjiga  i sadrži sledeće: 
-Dodatna polja autor i naslov tipa String
-Konstruktor oblika Katalogizacija (String autor, String naslov, String isbn,  Klasifikacija klasifikacija)
-Metod oblika void toString() za ispis svih polja klase Katalogizacija.  
Napisati klasu Test2 koja pored metode main sadrži sledeće: 
-Inicijalizaciju objekta klase Katalogizacija i za taj objekat pozvati metode ispis() iz klase Knjiga i toString() iz klase Katalogizacija