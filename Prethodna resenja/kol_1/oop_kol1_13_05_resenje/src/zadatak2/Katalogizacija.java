package zadatak2;

import zadatak1.Klasifikacija;
import zadatak1.Knjiga;

public class Katalogizacija extends Knjiga {
	String autor;
	String naslov;

	public Katalogizacija(String autor, String naslov, String isbn, Klasifikacija klasifikacija) {
		super(isbn, klasifikacija);
		this.autor = autor;
		this.naslov = naslov;
	}

	public void uString() {
		System.out.println("Katalogizacija [autor=" + autor + ", naslov=" + naslov + ", isbn=" + isbn
				+ ", klasifikacija=" + klasifikacija + "]");
	}

}
