package zadatak4;

public class Test4 {

	public static void main(String[] args) {
		UniverzitetSrbija us1 = new UniverzitetSrbija();
		us1.adresa = "Bulevar Mihajla Pupina 4a";
		us1.naziv = "Univerzitet Singidunum";
		
		UniverzitetSrbija us2 = new UniverzitetSrbija();
		us2.adresa = "Bulevar Cara Lazara 4";
		us2.naziv = "Drugi univerzitet";
		
		System.out.println(us1.podaciUniverzitet());
		System.out.println(us2.podaciUniverzitet());
		
		System.out.println(us1.skolarina());
		System.out.println(us2.skolarina());
		
		UniverzitetSrbija[] univerziteti = new UniverzitetSrbija[] {us1, us2};
		System.out.println("Univerzitet us2 postoji u univerziteti: " + us2.sadrziUniverzitet(univerziteti));

	}

}
