package zadatak4;

public class UniverzitetSrbija implements Univerzitet {
	String naziv;
	String adresa;

	@Override
	public float skolarina() {
		return 1500;
	}

	@Override
	public String podaciUniverzitet() {
		return "UniverzitetSrbija [naziv=" + naziv + ", adresa=" + adresa + "]";
	}

	boolean sadrziUniverzitet(UniverzitetSrbija[] univerziteti) {
		for (UniverzitetSrbija univerzitet : univerziteti) {
			if (univerzitet.naziv.equals(naziv)) {
				return true;
			}
		}
		return false;
	}
	

}
