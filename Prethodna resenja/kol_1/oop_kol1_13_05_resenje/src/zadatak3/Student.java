package zadatak3;

public class Student extends Osoba {
	int idFakulteta;

	public Student(String ime, String prezime, int jmbg, int idFakulteta) {
		super(ime, prezime, jmbg);
		this.idFakulteta = idFakulteta;
	}

	@Override
	void studira() {
		if (idFakulteta == 0) {
			System.out.println("Osoba ne studira");
		} else {
			System.out.println("Osoba studira");
		}

	}

	void uString() {
		System.out.println("Student [idFakulteta=" + idFakulteta + ", ime=" + ime + ", prezime=" + prezime + ", jmbg="
				+ jmbg + "]");
	}

}
