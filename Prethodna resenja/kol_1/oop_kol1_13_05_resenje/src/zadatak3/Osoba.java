package zadatak3;

public abstract class Osoba {
	String ime;
	String prezime;
	int jmbg;
	public Osoba(String ime, String prezime, int jmbg) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.jmbg = jmbg;
	}
	
	abstract void studira();
	
	void promenaPrezimena(String novoPrezime) {
		this.prezime = novoPrezime;
	}
}
