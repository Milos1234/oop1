package zadatak3;

public class Test3 {

	public static void main(String[] args) {
		Student s1 = new Student("Marko", "Markovic", 54523451, 0);
		Student s2 = new Student("Janko", "Jankovic", 5453123, 1);

		s1.studira();
		s2.studira();

		s1.uString();
		s2.uString();

		s1.promenaPrezimena("Maric");
		s1.uString();

	}

}
