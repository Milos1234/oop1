package grupa3.zadatak2;

public class Test32 {

	public static void main(String[] args) {
		Osoba vlasnik = new Osoba("Pera", "Tot");
		Automobil hecbek = new Automobil(vlasnik, "NS001NS", "WXG12314GW", Karoserija.HECBEK);
		Automobil limuzina = new Automobil(new Osoba("Marko", "Mirkovic"), "NS993NS", "G3GDR2523", Karoserija.LIMUZINA);
		Automobil karavan = new Automobil(vlasnik, "NS992NS", "IR4343KN", Karoserija.KARAVAN);
		
		hecbek.registruj(vlasnik);
		limuzina.registruj(vlasnik);
		karavan.registruj(new Osoba("Marko", "Mirkovic"));
	}

}
