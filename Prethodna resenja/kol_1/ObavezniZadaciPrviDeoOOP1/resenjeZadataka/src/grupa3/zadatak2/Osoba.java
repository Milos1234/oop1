package grupa3.zadatak2;

import java.util.Objects;

public class Osoba {
	String ime;
	String prezime;

	public Osoba() {}

	public Osoba(String ime, String prezime) {
		this.ime = ime;
		this.prezime = prezime;
	}

	// Ovo vazi u slucaju da se zahteva u metodi registruj i poredjenje vlasnika
	@Override
	public int hashCode() {
		return Objects.hash(ime, prezime);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Osoba other = (Osoba) obj;
		return Objects.equals(ime, other.ime) && Objects.equals(prezime, other.prezime);
	}
}
