package grupa3.zadatak1;

public class Radnik extends Osoba {
	private int idFirme;
	
	public Radnik(String ime, String pol, int idFirme) {
		super(ime, pol);
		this.idFirme = idFirme;
	}

	@Override
	public void zaposlen() {
		if (idFirme == 0) {
			System.out.println("Nije zaposlen.");
		} else {
			System.out.println("Jeste zaposlen.");
		}
	}
	
	public boolean sadrziIme(String s) {
		return ime.contains(s);
	}
}
