package grupa3.zadatak1;

public class Test31 {

	public static void main(String[] args) {
		Osoba r1 = new Radnik ("Marko", "Muško", 0);
		Osoba r2 = new Radnik ("Ana Anić", "Žensko", 54634);	
		r1.zaposlen();
		System.out.println(r1.toString());
		r2.zaposlen();
		System.out.println(r2.toString());
		r2.promenaImena("Ana Milić");
		System.out.println(r2.toString());
		Radnik r3 = new Radnik("Ana Marija Marinkovic", "Žensko", 5454);
		System.out.println("Sadrzi Ana? " + r3.sadrziIme("Ana"));
	}

}
