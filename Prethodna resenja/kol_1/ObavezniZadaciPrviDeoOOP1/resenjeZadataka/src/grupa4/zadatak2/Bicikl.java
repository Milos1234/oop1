package grupa4.zadatak2;

public interface Bicikl {
	void promenaBrzineNaMenjacu(int broj);
	void ubrzaj(int povecajZa);
	void koci(int smanjiZa);
}
