package grupa1.zadatak3;

public class Test13 {

	public static void main(String[] args) {
	    Student s1 = new Student("Pera","Perić", Smer.SII);  
	    Student s2 = new Student("Ana","Anić", Smer.IT);  
	    Student s3 = new Student("Vesna","Rot", Smer.SII);
	    Student.grupa ="Grupa IV";
	    s1.ispis();  
	    s2.ispis();  
	    s3.ispis();
	    Student.grupa ="Grupa II";
	    s1.ispis();  
	    s2.ispis();  
	    s3.ispis();
	}

}
