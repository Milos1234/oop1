package grupa1.zadatak2;

public class Osoba {
	private String ime, prezime;

	public Osoba(String ime, String prezime) {
		this.ime = ime;
		this.prezime = prezime;
	}

	String imePrezime() {
		return this.ime + " " + this.prezime;
	}

	String imePrezime(String ime, String prezime) {
		return ime + " " + prezime;
	}

	public String getIme() {
		return ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	public void ispis(Zaposlenost z) {
		System.out.format("Osoba ime: %s prezime: %s zaposlenost: %s", ime, prezime, z);
	}
}
